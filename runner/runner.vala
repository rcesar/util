using Gtk;

extern Gtk.ListStore * lista_arquivos();// a listagem de arquivos é feita no codigo C
extern void executar(char *pt);

class MainWindow: Window 
{
    private TreeView lista = new TreeView();

    public MainWindow()
    {
        this.title="Utilitarios Python";
        this.set_default_size(640,480);
        this.destroy.connect(Gtk.main_quit);
        //
        var wbase= new Box(Orientation.VERTICAL,0);
        var sb = new ScrolledWindow(null,null);
        this.add(wbase);
        sb.add(this.lista);
        this.lista.insert_column_with_attributes (-1, "",new Gtk.CellRendererText (), "text",0);
        this.lista.row_activated.connect(this.rodar);
        this.lista.set_headers_visible(false);
        wbase.pack_start(sb,true,true,0);
        var bt = new Button.with_label("Atualizar");
        bt.clicked.connect(()=>{this.lista.set_model(lista_arquivos());});
        wbase.pack_end(bt,false,false,0);
        this.lista.set_model(lista_arquivos());
    }
    private void rodar()
    {
        var md=this.lista.get_model();
        Gtk.TreeIter iter; //= new  Gtk.TreeIter();
        var cc = this.lista.get_selection().get_selected( out md,out iter);
        if (!cc) {
            stderr.printf("nada selecionado!\nComo nós chegamos aqui?");
            }
        Value valor;
        md.get_value(iter,0,out valor); 
        string appname=(string) valor;
        string pname="/usr/bin/python "+appname+"&";
        executar(pname);
    }
}

public static int main(string[] args)
{
    Gtk.init(ref args);
    MainWindow jn = new MainWindow();
    try
    {
    var pt = Path.get_dirname(args[0])+Path.DIR_SEPARATOR_S+"icone.png";
    var icone = new Gdk.Pixbuf.from_file(pt);
    jn.set_icon(icone);        
    } catch(Error e) {
        stderr.printf(e.message);
    }
    jn.show_all();
    Gtk.main();
    return(0);
}
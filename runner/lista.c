#include<glob.h>
#include<gtk/gtk.h>

void executar( char *pt)
{
    system(pt);
}
GtkListStore  * lista_arquivos()
{
    GtkListStore * itens = gtk_list_store_new(1,G_TYPE_STRING);
    GtkTreeIter iter; // nescessario para "gravar" os valores
    int indice=0;
    char *item;
    glob_t arquivos;
    //Lista os arquivos com extencao py
    memset(&arquivos, 0, sizeof(arquivos));
    char *path = getenv("HOME");
    char *cpath=(char*)malloc((strlen(path)+10)*sizeof(char));
    sprintf(cpath,"%s/util/*.py",path);
    int ret = glob(cpath, GLOB_TILDE, NULL, &arquivos);
    free(cpath);
    if(ret != 0) 
    {
        globfree(&arquivos);
     	puts("Erro listando arquivos");
    }
    for(size_t i = 0; i < arquivos.gl_pathc; ++i) {
        item=(char*)(arquivos.gl_pathv[i]);
        gtk_list_store_append (itens, &iter);
        gtk_list_store_set (itens, &iter,0,item,-1);
    }
    globfree(&arquivos); 
    //Lista os arquivos com extenção pyw
    memset(&arquivos, 0, sizeof(arquivos));
    cpath=(char*)(char*)malloc((strlen(path)+20)*sizeof(char));
    sprintf(cpath,"%s/util/*.pyw",path);
    ret = glob(cpath, GLOB_TILDE, NULL, &arquivos);
    if(ret != 0) 
    {
        globfree(&arquivos);
  	    puts("Erro listando arquivos");
    }
    for(size_t i = 0; i < arquivos.gl_pathc; ++i) {
        item=(char*)(arquivos.gl_pathv[i]);
        gtk_list_store_append (itens, &iter);
        gtk_list_store_set (itens, &iter,0,item,-1);

    }
    globfree(&arquivos);    
    return(itens);
}

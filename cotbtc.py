#-*-coding:utf-8-*-
#Calculadora de conversão bitcoins/reais
#Utiliza a API do MercadoBitcoin
#
from tkinter.tix import *
from tkinter import LabelFrame
import time, requests,sys

URL="http://www.mercadobitcoin.net/api/BTC/ticker/"

class CotBTC(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.title("Cotação do bitcoin")
        fr=[LabelFrame(self,text="Cotação:"),LabelFrame(self,text="Valor em Satochis( btc/100.000.000):"),LabelFrame(self,text="Valor em reais:")]
        self.cotacao=Entry(fr[0],font=('Arial',32),fg="#FF2222");self.cotacao.pack(side='top',fill='both',expand=True)
        self.sts=Entry(fr[1],font=('Arial',18));self.sts.pack(side='top',fill='both',expand=True)
        self.sts.bind("<Return>",self.atualiza_rs)
        self.rs=Entry(fr[2],font=('Arial',18));self.rs.pack(side='top',fill='both',expand=True)
        self.rs.bind("<Return>",self.atualiza_sts)
        fr[0].pack(side='top',fill='x')
        fr[1].pack(side='top',fill='x')
        fr[2].pack(side='top',fill='x')
        tb=Frame(self);tb.pack(side='top',fill='x')
        Button(tb,text="Atualizar").pack(side='left',fill='x')
        Button(tb,text="Sair",command=lambda:self.destroy()).pack(side='left',fill='x')
        pstat=Frame(self,relief='ridge',border=3)
        Label(pstat,text="Ultima atualização:").pack(side='left')
        self.at=Label(pstat,text='NEVER',relief='ridge');self.at.pack(side='left')
        self.lrelogio=Label(pstat,relief="ridge",text=time.strftime("%H:%M:%S"))
        self.lrelogio.pack(side='right')
        pstat.pack(side='bottom',fill='x')
        self.relogio()

    def atualiza_rs(self,*args):
        try:
            valor=self.sts.get()
            for c in valor:
                if '0123456789,.'.find(c)<0:
                    valor.replace(c,'')
            valor=valor.replace('.','').replace(',','.')
            if valor.strip()=='':return
            cotacao=float(self.cotacao.get().replace('.','').replace(",","."))
            sts=float(valor)
            reais=formata_moeda(('%.2f'%((sts/1000)*cotacao)).replace(".",","))
            self.rs.delete('0','end')
            self.rs.insert('end',reais)
        except  Exception as e:
            self.eval("tk_messageBox -message {%s} -icon error  "%str(e))

    def atualiza_sts(self,*args):
        try:
            valor=self.rs.get()
            for c in valor:
                if '0123456789,.'.find(c)<0:
                    valor.replace(c,'')
            valor=valor.replace('.','').replace(',','.')
            if valor.strip()=='':return
            cotacao=float(self.cotacao.get().replace('.','').replace(",","."))
            reais=float(valor)
            satoshi=(reais/cotacao)*1000.00
            self.sts.delete('0','end')
            self.sts.insert('end',('%.10f'%satoshi).replace(".",","))
        except  Exception as e:
            #d_erros=(sys.exc_info()[2])#mostra o numero da linha onde houve erro
            #print('%s -- Linha numero %i'%(str(e),d_erros.tb_lineno))
            self.eval("tk_messageBox -message {%s} -icon error  "%str(e))

    def relogio(self):
        self.lrelogio['text']=time.strftime("%H:%M:%S")
        self.after(1000,self.relogio)
        if self.at['text']=='NEVER':self.atualizar()

    def atualizar(self):
        self.cotacao.delete('0','end')
        cot=('%.2f'%float(requests.get(URL).json()['ticker']['last'])).replace(".",",")
        self.cotacao.insert('end',formata_moeda(cot))
        self.at['text']=(time.strftime("%H:%M:%S"))
        self.after(5000,self.atualizar)

        
def formata_moeda(valor):
    try:
        inteiro,decimal = valor.split(",")
    except:
        inteiro=valor
        decimal='00'
    inteiro = split1000(inteiro)
    return('%s,%s'%(inteiro,decimal))

def split1000(s):
   return s if len(s) <= 3 else split1000(s[:-3]) + '.' + s[-3:]
                                                            
if __name__=="__main__":
    CotBTC()
    mainloop()
            

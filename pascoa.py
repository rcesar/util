#-----------------------------------------------------------
#-*-encoding:utf-8-*-
# Algoritimo para calculo da data da pascoa 
# Baseado no algoritimo chamado de "Meeus/Jones/Butcher"
# Este algoritimo é valido para todos os anos do calendario gregoriano
# a partir de 1.523
# 26/12/2018 
#
from datetime import date
from tkinter import *

def pascoa(ano):
    dint=lambda v1,v2:int(v1/v2)
    a=ano%19
    b=dint(ano,100)
    c=ano%100
    d=dint(b,4)
    e=b%4
    f=dint((b+8),25)
    g=dint((b-f+1),3)
    h=(19*a+b-d-g+15)%30
    i=dint(c,4)
    k=c%4
    l=(32+2*e+2*i-h-k)%7
    m=dint((a+11*h+22*l),451)
    mes=dint((h+l-7*m+114),31)
    dia=((h+l-7*m+114)%31)+1
    return(date(ano,mes,dia))

class MW(Tk):
    def __init__(self):
        Tk.__init__(self)
        self.title("Calculo da data da pascoa")
        fr=LabelFrame(self,text="Ano:")
        fr.pack(side='top',fill='x')    
        frb=Frame(self)
        frb.pack(side='bottom',fill='x')
        self.ano=Entry(fr)
        self.ano.pack(side='top',fill='both',expand=True)
        Button(frb,text='Calcular',command=self.calcular).pack(side='left')
        Button(frb,text='Fechar',command=self.destroy).pack(side='right')
        
        
    def calcular(self):
        data=pascoa(int(self.ano.get()))
        msg="A pascoa em %s será em %.2i/%.2i/%s"%(self.ano.get(),data.day,data.month,data.year)
        self.tk.eval("tk_messageBox -message {%s}"%msg)
        
if __name__=="__main__":
     MW()
     mainloop()

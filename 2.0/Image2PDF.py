#
#-*-coding:utf-8-*-
#-------------------------------------
# This script uses image magick convert to convert multiple
# image files to one pdf, using grayscale and compress in zip format
# to reduce the size.
# Distributed in hope to be useful but with no warranty, under most recent 
# GPL  license on https://www.gnu.org/licenses/#GPL
# Version 1.0.0 dated 18/02/2019
#
import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from gi.repository.GdkPixbuf import Pixbuf
from PIL import Image
import sys,os,subprocess

app_name = "/usr/bin/convert"
config = ["-set","colorspace","gray","-compress","Zip"] #COmpress pode ser None, BZip, Fax, Group4, JPEG, JPEG2000, Lossless, LZW, RLE or Zip.


class Image2pdf(Gtk.Window):
    def __init__(self):
        Gtk.Window.__init__(self)
        self.set_title("Image 2 PDF")
        self.set_size_request(800,600)
        fr=Gtk.Frame(label="Sources:")
        scw=Gtk.ScrolledWindow()
        fr.add(scw)
        scw.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        self.fontes=Gtk.TreeView()
        self.fontes.append_column(Gtk.TreeViewColumn("",Gtk.CellRendererText(),text=0))
        self.fontes.set_model(Gtk.ListStore(str))
        scw.add(self.fontes)
        #
        img_mais=Gtk.Image();img_mais.set_from_pixbuf(Pixbuf.new_from_xpm_data(images["mais"]))
        img_menos=Gtk.Image();img_menos.set_from_pixbuf(Pixbuf.new_from_xpm_data(images["menos"]))
        img_run=Gtk.Image();img_run.set_from_pixbuf(Pixbuf.new_from_xpm_data(images["run"]))
        img_config=Gtk.Image();img_config.set_from_pixbuf(Pixbuf.new_from_xpm_data(images["config"]))
        tb=Gtk.Toolbar()
        bt=[Gtk.ToolButton(stock_id="gtk-quit"),
            Gtk.SeparatorToolItem(),
            Gtk.ToolButton(icon_widget=img_mais),
            Gtk.ToolButton(icon_widget=img_menos),
            Gtk.ToolButton(icon_widget=img_run)]#,SeparatorToolItem(),ToolButton(img_config)]
        bt[0].connect("clicked",lambda evt:self.destroy())
        bt[2].connect("clicked",lambda evt:self.add_file())
        bt[3].connect("clicked",lambda evt:self.remove_file())
        bt[4].connect("clicked",lambda evt:self.process())
        #bt[6].connect("clicked",lambda evt:self.config())
        for b in bt:tb.add(b)
        #
        fr2=Gtk.Frame(label="Destiny:")
        baseb=Gtk.HBox(homogeneous=False,spacing=0)
        img=Gtk.Image()
        img.set_from_pixbuf(Pixbuf.new_from_xpm_data(images["pasta"]))
        b=Gtk.ToolButton(icon_widget=img)
        b.connect("clicked",lambda evt:self.selec_destino())
        baseb.pack_end(b,False,False,0)   
        self.destino=Gtk.Entry()
        baseb.pack_start(self.destino,True,True,0)
        fr2.add(baseb)
        #
        base1=Gtk.VBox(homogeneous=False,spacing=0)
        base1.pack_start(tb,False,True,0)
        base1.pack_start(fr,True,True,0)
        base1.pack_end(fr2,False,True,0)
        #
        self.add(base1)

    def selec_destino(self):
        df = Gtk.FileChooserDialog(action=Gtk.FileChooserAction.SAVE,title="Salvar no arquivo:")
        df.buttons=("gtk-ok",1,"gtk-cancel",0)
        df.set_current_folder(os.environ.get("HOME"))
        ft = [Gtk.FileFilter(),Gtk.FileFilter(),Gtk.FileFilter()]
        ft[0].set_name("PDF")
        ft[0].add_pattern("*.pdf")
        ft[2].set_name("Todos os arquivos")
        ft[2].add_pattern("*.*")
        for f in ft:df.add_filter(f)
        if df.run():
            try:
                self.destino.set_text(df.get_filename())
            except:
                dlg=Gtk.MessageDialog(parent=self,message_type=Gtk.MessageType.ERROR,buttons=Gtk.ButtonsType.OK);dlg.set_markup("Nenhum arquivo selecionado")
                dlg.run();dlg.destroy()
        df.destroy()       

    def add_file(self):
        df = Gtk.FileChooserDialog(action=Gtk.FileChooserAction.OPEN,title="Selecione imagem de origem")
        df.add_buttons("gtk-ok",1,"gtk-cancel",0)
        df.set_select_multiple(True)
        ft = [Gtk.FileFilter(),Gtk.FileFilter(),Gtk.FileFilter()]
        ft[0].set_name("JPEG")
        ft[0].add_pattern("*.jpg")
        ft[1].set_name("PNG")
        ft[1].add_pattern("*.png")
        ft[2].set_name("Todos os arquivos")
        ft[2].add_pattern("*.*")
        for f in ft:df.add_filter(f)
        if df.run():
            try:
                for nome in df.get_filenames():
                    self.fontes.get_model().append([os.path.abspath(nome)])
            except:
                dlg=Gtk.MessageDialog(parent=self,message_type=Gtk.MessageType.ERROR,buttons=Gtk.ButtonsType.OK);dlg.set_markup("Nenhum arquivo selecionado")
                dlg.run();dlg.destroy()
        df.destroy()       

    def remove_file(self):
        md,it=self.fontes.get_selection().get_selected()
        if it:md.remove(it)

    def process(self):
        destino=self.destino.get_text()
        if destino=='':return
        dlg=Gtk.MessageDialog(parent=self,buttons=Gtk.ButtonsType.YES_NO,message_type=Gtk.MessageType.QUESTION)
        dlg.set_markup("<b>Create PDF file from selected images?</b>")
        if dlg.run()!=Gtk.ResponseType.YES:return
        imgs=[]
        md=self.fontes.get_model()
        it=md.get_iter_first()            
        if not(it):
            dlg=GTk.MessageDialog(parent=self,buttons=Gtk.ButtonsType.OK,message_type=Gtk.MessageType.ERROR)
            dlg.set_markup("No image on list")
            dlg.run();dlg.destroy()
            return
        while it:
            i=Image.open(os.path.abspath(md.get(it,0)[0]))
            i.convert("RGB")
            imgs.append(i)
        if len(imgs)<=1:
            imgs[0].save(destino,save_all=True)
        else:
            im1.save(destino,save_all=True, append_images=imagelist)
        

    def process_old(self):
        if self.destino.get_text().strip()=="":
            dlg=Gtk.MessageDialog(parent=self,buttons=Gtk.ButtonsType.OK,message_type=Gtk.MessageType.ERROR)
            dlg.set_markup("Não informado arquivo de destino")
            dlg.run();dlg.destroy()
            return
        md=self.fontes.get_model()
        it =md.get_iter_first()
        if not(it):
            dlg=GTk.MessageDialog(parent=self,buttons=Gtk.ButtonsType.OK,message_type=Gtk.MessageType.ERROR)
            dlg.set_markup("Nenhuma imagem na lista")
            dlg.run();dlg.destroy()
            return            
        dlg=Gtk.MessageDialog(parent=self,buttons=Gtk.ButtonsType.YES_NO,message_type=Gtk.MessgeType.QUESTION)
        dlg.set_markup("<b>Gerar PDF a partir das imagens selecionadas</b>")
        if dlg.run()==Gtk.ResponseType.YES:
            lista = [app_name]
            for c in config:lista.append(c)
            md=self.fontes.get_model()
            it=md.get_iter_first()
            while it:
                lista.append(md.get(it,0)[0])
                it=md.iter_next(it)
            lista.append(os.path.abspath(self.destino.get_text()))
            pid=subprocess.Popen(lista,stdout=subprocess.PIPE,stderr=subprocess.PIPE)
            ret=pid.stdout.read()
            err=pid.stderr.read()
            message=ret+err
            if message.strip()!="":
                dl=GTk.MessageDialog(parent=self,buttons=Gtk.ButtonsType.OK,message_type=Gtk.MessageType.INFO);dl.set_markup(message)
                dl.run();dl.destroy()
            pid.communicate()
        dlg.destroy()


images ={"mais":["24 24 2 1",
" 	c None",
".	c #000000",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"           ..           ",
"           ..           ",
"           ..           ",
"           ..           ",
"           ..           ",
"           ..           ",
"     ..............     ",
"     ..............     ",
"           ..           ",
"           ..           ",
"           ..           ",
"           ..           ",
"           ..           ",
"           ..           ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        "],
"menos":["24 24 2 1",
" 	c None",
".	c #000000",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"     ..............     ",
"     ..............     ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        " ],
"pasta":["24 24 7 1",
" 	c None",
".	c #191918",
"+	c #F6E310",
"@	c #D6C610",
"#	c #292614",
"$	c #D7C710",
"%	c #C5B710",
"                        ",
"                        ",
"                        ",
"                        ",
"      ................  ",
" ......++++++++++++++.  ",
" .+++..++++++++++++++.  ",
" .+++.............+++.  ",
"  .++++++++++++++@.++.  ",
"  .+++++++++++++++.++.  ",
"  .+++++++++++++++#++.  ",
"   .++++++++++++++$.+.  ",
"   .+++++++++++++++.+.  ",
"   .+++++++++++++++.+.  ",
"    .++++++++++++++%..  ",
"    .+++++++++++++++..  ",
"    .+++++++++++++++..  ",
"     .+++++++++++++++.  ",
"     .+++++++++++++++.  ",
"     .................  ",
"                        ",
"                        ",
"                        ",
"                        "],
"run":["24 24 2 1",
" 	c None",
".	c #000000",
"         ......         ",
"         ......         ",
"    ...  ......    .    ",
"   ..... ......  .....  ",
"  ............. ....... ",
"  ..................... ",
"  ....................  ",
"    ..... ....  .....   ",
"     ...  ....   ....   ",
"  .....   ...   ........",
"......     .............",
".........  .............",
"........... . ..........",
".........  ..   ........",
"......     ..    .....  ",
"   ....   ....  ...     ",
"   .....  .... .....    ",
"  ....................  ",
" .....................  ",
" ....... .............  ",
"  .....  ...... .....   ",
"    .    ......  ...    ",
"         ......         ",
"         ......         "],
"config":["24 24 2 1",
" 	c None",
".	c #000000",
"                        ",
"                        ",
" ...................... ",
" . ..     ..    ..    . ",
" .   ..     ..    ..  . ",
" .     ..     ..    ... ",
" .................... . ",
" .                    . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" . ...   ...  ... ... . ",
" .    ..    ..  ..    . ",
" ...................... ",
"                        "]}

if __name__=="__main__":
    jn=Image2pdf()
    jn.connect("destroy",Gtk.main_quit)
    jn.show_all()
    Gtk.main()

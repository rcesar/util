#
#-*-coding:utf-8-*-

from tkinter import *
import base64
import webbrowser
#rar.odalbuD.2991.s13v40dr3pm1.s0/sih07vu7/ot.lu//:ptth

class MW(Tk):
    def __init__(self):
        Tk.__init__(self)
        fr=LabelFrame(self,text="Origem:")
        fr1=LabelFrame(self,text="Destravado:")
        self.ori=Entry(fr)
        self.ori.pack(side='top',fill='both',expand=True)
        self.dest=Entry(fr1)
        self.dest.pack(side='top',fill='both',expand=True)
        fr.pack(side='top',fill='x')
        fr1.pack(side='top',fill='x')
        fr=Frame(self)
        self.geometry("600x300")
        self.wm_title("Destravador de url de download")     
        fr.pack(side='bottom',fill='x')
        Button(fr,text='Base64',command=self.base64).pack(side='left')
        Button(fr,text='Inverter',command=self.inverter).pack(side='left')
        Button(fr,text='Abrir no navegador',command=lambda:webbrowser.open_new_tab(self.dest.get())).pack(side='left')
        Button(fr,text="Limpar",command=lambda:self.limpar()).pack(side='left') 
        Button(fr,text="Sair",command=self.destroy).pack(side='left')

        def limpar(self):
                self.dest.delete(0,'end')
                self.ori.delete(0,'end')

    def base64(self):
        try:
            self.dest.insert('end',base64.decodebytes(bytes(self.ori.get(),"utf-8")))
        except Exception as e:
            print(e)
            self.tk.eval('tk_messageBox  -message {Texto original não esta em formato "BASE64"}')
            
    def inverter(self):
        try:
            saida=''
            for c in range(len(self.ori.get())-1,-1,-1):saida+=self.ori.get()[c]
            self.dest.insert('end',saida)
        except Exception as e:
            print(e)
            self.tk.eval('tk_messageBox  -message {Erro invertendo texto %s}'%(str(e)))
        

if __name__=="__main__":
    MW()
    mainloop()

#
#-*-coding:utf-8-*-
#
import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
import qrcode,tempfile,os

_=lambda str:str

class WQrCode(Gtk.Window):
    def __init__(self):
        self.qr=None#future qrcode object
        self.error_correction_levels=[]
        for name in dir(qrcode.constants):
            if name[0:2]!="__":self.error_correction_levels.append(name)#put all constants in a var so can choose later from user input
        Gtk.Window.__init__(self)
        self.set_size_request(800,600)
        self.set_title(_("QrCode generator"))
        base=Gtk.Box(orientation=Gtk.Orientation.VERTICAL,spacing=0,homogeneous=False)
        fr=Gtk.Frame(label=_("Data:"))
        tbd=Gtk.Box(orientation=Gtk.Orientation.HORIZONTAL,spacing=0)#container with qrcode option widgets
        #Version
        lversion=Gtk.Frame(label=_("Version:"))
        self.version=Gtk.SpinButton.new_with_range(1,40,1)
        lversion.add(self.version)
        #Box size
        lsize=Gtk.Frame(label=_("Box size:"))
        self.size=Gtk.SpinButton.new_with_range(10,100,1)
        lsize.add(self.size)
        #Border size
        lsize2=Gtk.Frame(label=_("Border size:"))
        self.bordersize=Gtk.SpinButton.new_with_range(1,5,1)
        lsize2.add(self.bordersize)
        #Error level combobox creation
        lerror=Gtk.Frame(label=_("Erro correcting level:"))
        st=Gtk.ListStore(int,str)
        st.append([0,"L 7%"])
        st.append([1,"M 15%"])  
        st.append([2,"Q 25%"])
        st.append([3,"H 30%"])
        self.error=Gtk.ComboBox.new_with_model(st)
        renderer=Gtk.CellRendererText()        
        self.error.pack_start(renderer,True)
        self.error.add_attribute(renderer, "text", 1)
        self.error.set_active(2)
        lerror.add(self.error)
        #Data entry text editor
        self.data=Gtk.TextView()
        scw=Gtk.ScrolledWindow()
        scw.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        scw.add(self.data)
        #Generate button
        genbt=Gtk.ToolButton(stock_id="gtk-execute")
        genbt.connect("clicked",lambda et:self.generate_qr())
        #
        #tbd.pack_start(lversion,False,True,1) #Version is better set automatic from FIT property when generating qrcode
        tbd.pack_start(lsize,False,True,1)
        tbd.pack_start(lsize2,False,True,1)
        tbd.pack_start(lerror,False,True,0)
        b1=Gtk.Box(orientation=Gtk.Orientation.VERTICAL)#base container for data widgets
        b1.pack_start(scw,True,True,1)
        b1.pack_start(tbd,False,True,0)
        b1.pack_end(genbt,False,True,0)
        fr.add(b1)
        #
        self.img=Gtk.Image()
        fr2=Gtk.Frame(label="QRCode:")
        scw1=Gtk.ScrolledWindow()
        scw1.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        scw1.add(self.img)
        fr2.add(scw1)
        #toobar
        tb=Gtk.Toolbar()
        btsai=Gtk.ToolButton(stock_id="gtk-quit");btsai.connect("clicked",lambda evt:self.destroy())
        btsalva=Gtk.ToolButton(stock_id="gtk-save");btsalva.connect("clicked",lambda evt:self.salvar())
        tb.add(btsai)
        tb.add(Gtk.SeparatorToolItem())
        tb.add(btsalva)
        #
        base.pack_start(fr,True,True,0)
        base.pack_start(fr2,True,True,0)
        base.pack_end(tb,False,True,2)
        self.add(base)
    

    def generate_qr(self):
        data=self.data.get_buffer().get_text(self.data.get_buffer().get_start_iter(),self.data.get_buffer().get_end_iter(),0)
        error=self.error_correction_levels[self.error.get_active()]
        size=int(self.size.get_value())
        border=int(self.bordersize.get_value())
        self.qr=qrcode.QRCode(
            version=None,
            error_correction=getattr(qrcode.constants,error),
            box_size=size,
            border=border
        )
        self.qr.add_data(data)
        self.qr.make(fit=True)# fit=true will adjust the qrcode version automatic
        img=self.qr.make_image(fill_color="black",back_color="white")
        name=tempfile.mktemp(".png")
        img.save(name)
        self.img.set_from_file(name)
        os.remove(name)#delete file

    def salvar(self):
        ffilter=Gtk.FileFilter()
        ffilter.set_name("Imagens PNG")
        ffilter.add_pattern("*.png")
        dlg=Gtk.FileChooserDialog(title="Salvar",parent=self,action=Gtk.FileChooserAction.SAVE)
        dlg.add_buttons("gtk-ok",1,"gtk-cancel",0)
        dlg.add_filter(ffilter)
        if self.qr and dlg.run():
            try:
                img=self.qr.make_image(fill_color="black",back_color="white")
                img.save(dlg.get_filename())
            except Exception(e):
                print("Erro salvando arquivo:%s"%e)
        dlg.destroy()
                    

if __name__=="__main__":
    mw=WQrCode()
    mw.connect("destroy",Gtk.main_quit)
    mw.show_all()
    Gtk.main()

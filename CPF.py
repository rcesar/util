#
#-*-coding:utf-8-*-
# Biblioteca que permite validar cpf e cnpj de acordo com 
# o algoritimo utilizado pela receita federal do Brasil
# para criação de digito verificador.
# Data da versão 01.07.2019
# Rodrigo Cesar Herefeld (rcesarh27@gmail.com)
# Biblioteca disponibilizada de acordo com os termos
# da Licença Pública Geral Menor GNU Versão 3.0
# disponivel no site (https://www.gnu.org/licenses/lgpl-3.0.html)
#=================================================
class CPF:
    def __init__(self,pad=''):
        if pad=='':return

    def tipo(self,valor):
        valor=valor.replace(".","").replace("-","").replace("/","")
        valor=valor[:-2]
        if len(valor)<=9:
            return("CPF")
        else:
            return("CNPJ")
        
    def __call__(self,cpf):
        cpf=cpf.replace(".","").replace("-","").replace("/","")
        dv=cpf[-2:]
        cpf=cpf[:-2]
        if len(cpf)<=9:
            ndv=self.calcdv_cpf(cpf)
            if ndv==dv:
                return(True)
            else:
                return(False)
                print(dv)
        else:
            ndv=self.calcdv_cnpj(cpf)
            if ndv==dv:
                return(True)
            else:
                return(False)

    def calcdv_cnpj(self,valor):
        m=2
        dv=0
        for i in range(len(valor)-1,-1,-1):
            dv+=int(valor[i])*m
            #print("m=%i x v=%s (indice %.2i)=>%i"%(m,valor[i],i,dv))
            if m<9:
                m+=1
            else:
                m=2
        if dv%11<2:
            dv=0
        else:
            dv=11-dv%11
        m=2
        dv2=0
        valor="%s%i"%(valor,dv)
        for i in range(len(valor)-1,-1,-1):
            dv2+=int(valor[i])*m
            #print("m=%i x v=%s (indice %.2i)=>%i"%(m,valor[i],i,dv2))
            if m<9:
                m+=1
            else:
                m=2
        if dv2%11<2:
            dv2=0
        else:
            dv2=11-dv2%11
        return("%i%i"%(dv,dv2))

    def calcdv_cpf(self,cpf):
        cpf=str(cpf)
        d1=self.mod11(cpf)
        d2=self.mod11(cpf+str(d1),11)
        return("%i%i"%(d1,d2))
    
    def mod11(self,pad,m=10):
        soma=0
        for i in range(0,len(pad)):
            soma+=int(pad[i])*m
            m-=1
        dv=soma%11
        if dv >1:
            dv=11-dv
        else:dv=0
        return(dv)

def testar(jn,cpf):
        c=CPF()
        if c(cpf):
            jn.tk.eval("tk_messageBox -message {%s %s é válido}"%(c.tipo(cpf),cpf))
        else:
            jn.tk.eval("tk_messageBox -message {%s %s é inválido}"%(c.tipo(cpf),cpf))

if __name__=="__main__":
    from tkinter import *
    cpf=CPF()
    jn=Tk()
    jn.wm_geometry("400x200")
    jn.title("Validador de CPF/CNPJ")
    fr=LabelFrame(jn,text="CPF/CNPJ:");fr.pack(side='top',fill='x')
    cpf=Entry(fr)
    cpf.pack(side='top',fill='both')
    tb=Frame(jn);tb.pack(side='bottom',fill='x')
    Button(tb,text="Validar",command=lambda:testar(jn,cpf.get())).pack(side='left')
    Button(tb,text="Fechar",command=lambda:jn.destroy()).pack(side='left')
    mainloop()
#    c=CPF()
#    cnpj=["00.000.000/0001-91","95.164.648/0001-00","20.250.316/0001-49","41.591.503/0001-66","99.999.999/0001-22",
#          "222.333.444-05","123.444.555-74","345.556.777-06","345.666.789-93"]
#    for doc in cnpj:

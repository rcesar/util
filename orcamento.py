#
#-*-coding:utf-8-*-
import gi
gi.require_version("Gtk","3.0")
from gi.repository import Gtk
from gi.repository.Gtk import HBox,Window,ToolButton,Button,main_quit,main,Dialog,ListStore,Label,AccelGroup,Box
from gi.repository.Gtk import CellRendererText,Menu,SeparatorMenuItem,ImageMenuItem,TreeView,ScrolledWindow,TreeViewColumn
from gi.repository.Gtk import Frame,VBox,MenuBar,Toolbar,SeparatorToolItem,Notebook,Statusbar,Image,MenuItem,Entry
from gi.repository import Gdk as gdk
from gi.repository.GdkPixbuf import Pixbuf

import time,os,json,datetime
from sqlite3 import dbapi2 as sqlite
import re
#
class Fixos(Frame):
    def __init__(self,parent):
        Frame.__init__(self)
        base = VBox(homogeneous=False,spacing=False)
        self.db=parent.db
        self.top=parent
        swc= ScrolledWindow()
        swc.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        self.lista=TreeView()
        swc.add(self.lista)
        self.lista.append_column(TreeViewColumn("Dia",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Descrição",CellRendererText(),text=1))
        self.lista.append_column(TreeViewColumn("Valor",CellRendererText(),text=2)) 
        self.lista.get_columns()[0].set_min_width(100)
        self.lista.get_columns()[1].set_min_width(400)
        self.lista.get_columns()[2].set_max_width(200)
        self.lista.connect("row-activated",self.editar)
        tb = Toolbar()
        bts = [ToolButton(stock_id='gtk-close'),SeparatorToolItem(),ToolButton(stock_id='gtk-new'),SeparatorToolItem(),ToolButton(stock_id='gtk-delete')]
        for bt in bts:tb.add(bt)
        bts[0].connect("clicked",lambda evt:self.destroy())
        bts[2].connect("clicked",self.novo)
        bts[4].connect("clicked",self.apagar)
        self.lista.set_model(ListStore(str,str,str))       
        self.atualizar()
        base.pack_start(swc,True,True,1)
        base.pack_end(tb,False,True,1)
        self.add(base)
        self.top.desktop.set_current_page(-1)

    def novo(self,evt):
        dlg=dlgFixo(self)
        if dlg.run():
            try:
                dia = dlg.dia.get_text()
                descricao = dlg.descricao.get_text()
                valor = float(dlg.valor.get_text())
                if self.db.execute("SELECT count(descricao) FROM gastos_fixos WHERE descricao=? AND dia=?",(descricao,dia)).fetchone()[0]<=0:
                    self.db.execute("INSERT INTO gastos_fixos VALUES(?,?,?)",(dia,descricao,valor))
                    self.db.commit()
                    self.atualizar()
                else:
                    msg = MessageDialog(parent=self.top,message_type=Gtk.MessageType.INFOERROR,buttons=BUTTONS_OK)
                    msg.set_markup("<b>Registro já existe!</b>")
                    msg.run();msg.destroy()
            except Exception as e:
                errmsg = Gtk.MessageDialog(parent=self.top,message_type=Gtk.MessageType.ERROR,buttons=BUTTONS_OK)
                errmsg.set_markup("<b> ERRO:\n%s</b>"%str(e))
                errmsg.run();errmsg.destroy()
        dlg.destroy()
        
    def apagar(self,evt):
        md,it = self.lista.get_selection().get_selected()
        if it==None or md[it][1]=='Total...:':return
        dlg = Gtk.MessageDialog(parent=self.top,message_type=Gtk.MessageType.QUESTION,buttons=Gtk.ButtonsType.YES_NO)
        dlg.set_markup("Apagar o gasto:\n%s ?"%md[it][1])
        if dlg.run()==Gtk.ResponseType.YES:
            self.db.execute("DELETE FROM gastos_fixos WHERE dia=? AND descricao=?",(md[it][0],md[it][1]))
            self.db.commit()
            self.atualizar()
        dlg.destroy()

    def editar(self,*args):
        md,it = self.lista.get_selection().get_selected()
        if it==None or md[it][1]=='Total...:':return
        dia = md[it][0]
        descricao = md[it][1]
        valor = md[it][2]
        dlg = dlgFixo(self)
        dlg.dia.set_text(dia)
        dlg.descricao.set_text(descricao)
        dlg.valor.set_text(valor)
        if dlg.run():
            try:
                self.db.execute("UPDATE gastos_fixos SET dia=?,descricao=?,valor=? WHERE dia=? AND descricao=? AND valor=?",(dlg.dia.get_text(),dlg.descricao.get_text(),float(dlg.valor.get_text()),dia,descricao,valor))
                self.db.commit()
                self.atualizar()
            except Exception as e:
                errmsg = Gtk.MessageDialog(parent=self.top,message_type=Gtk.MessageType.ERROR,buttons=Gtk.ButtonsType.OK)
                errmsg.set_markup("<b> ERRO:\n%s</b>"%str(e))
                errmsg.run();errmsg.destroy()
        dlg.destroy()
        
    def atualizar(self):
        md = ListStore(str,str,str)
        total=0
        for campo in self.db.execute("SELECT dia,descricao,valor FROM gastos_fixos ORDER BY dia,descricao").fetchall():
            saida = []
            for c in campo:saida.append('%s'%c)
            total+=float(saida[2])
            saida[2]='%.2f'%float(saida[2])
            md.append(saida)
        md.append(['','Total...:','%.2f'%float(total)])
        self.lista.set_model(md)

class dlgFixo(Dialog):
    def __init__(self,parent):
        Dialog.__init__(self,"Gastos fixos",parent.top)
        self.add_buttons("gtk-ok",1,"gtk-cancel",0)
        frs  = [Frame(label="Dia:"),Frame(label="Descrição:"),Frame(label="Valor:")]
        self.dia = Entry();frs[0].add(self.dia)
        self.descricao=Entry();frs[1].add(self.descricao)
        self.valor=Entry();frs[2].add(self.valor)
        for fr in frs:self.vbox.pack_start(fr,False,False,1)
        self.show_all()
#
class Parcelamentos(Frame):
    def __init__(self,parent):
        Frame.__init__(self)
        self.db=parent.db
        self.top=parent
        swc= ScrolledWindow()
        swc.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        tb = self.make_toolbar()
        bx =VBox(homogeneous=False,spacing=0)
        self.add(bx)
        bx.pack_start(tb,False,True,1)
        bx.pack_end(swc,True,True,1)
        self.lista=TreeView()
        swc.add(self.lista)
        self.lista.append_column(TreeViewColumn("Data",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Descrição",CellRendererText(),text=1))
        self.lista.append_column(TreeViewColumn("Valor",CellRendererText(),text=2))        
        self.lista.append_column(TreeViewColumn("Dt. prim. parcela",CellRendererText(),text=3))
        self.lista.append_column(TreeViewColumn("Dt. ultima parcela",CellRendererText(),text=4))                                                
        self.lista.append_column(TreeViewColumn("Num. parc.",CellRendererText(),text=5))
        self.lista.append_column(TreeViewColumn("Val. parc.",CellRendererText(),text=6))
        self.lista.append_column(TreeViewColumn("Saldo",CellRendererText(),text=7))    
        self.lista.get_columns()[1].set_min_width(200)
        self.lista.get_columns()[2].set_min_width(100)
        self.atualizar()
        self.top.desktop.set_current_page(-1)

    def novo(self):
        dlg = ParcelamentosDLG()
        if dlg.run():
            data_final = soma_mes(dlg.datainicio.get(),int(dlg.numparcelas.get_value()))
            dados = (dlg.data.get(),dlg.descricao.get_text(),dlg.valor.get_text(),dlg.datainicio.get(),data_final,int(dlg.numparcelas.get_value()))
            count=self.db.execute("SELECT count(descricao) FROM parcelamentos WHERE data=? AND descricao=? AND valor=? AND datainicio=? AND datafinal=? AND numparcelas=?",dados).fetchone()[0]
            if count==0 and dados[1].strip()!='':
                self.db.execute("INSERT INTO parcelamentos(data,descricao,valor,datainicio,datafinal,numparcelas) VALUES(?,?,?,?,?,?)",dados)
                self.db.commit()
                self.atualizar()
            else:
                dl=MessageDialog(parent=self.top,buttons=BUTTONS_OK)
                dl.set_markup("<b>Já existe parcelamento</b>")
                dl.run();dl.destroy()
        dlg.destroy()
        
    def editar(self):
        dlg = ParcelamentosDLG()
        if dlg.run():
            print("salvar")
        dlg.destroy()

    def apagar(self):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        dados = [md[it][0],md[it][1],md[it][2],md[it][3],md[it][4],md[it][5]]
        dlg = Gtk.MessageDialog(parent=self.top,buttons=Gtk.ButtonsType.YES_NO)
        dlg.set_markup("<b>Apagar os dados selecionados ?</b>")
        if dlg.run()==Gtk.ResponseType.YES:            
            self.db.execute("DELETE FROM parcelamentos WHERE data=? AND descricao=? AND valor=? AND datainicio=? AND datafinal=? AND numparcelas=?",dados)
            self.db.commit()
            self.atualizar()
        dlg.destroy()

    def atualizar(self):
        md = ListStore(str,str,str,str,str,str,str,str)
        hoje = time.strftime("%Y-%m-%d")
        soma_parcela = 0.00
        soma_saldo = 0.00
        soma_total= 0.00
        for campo in self.db.execute("SELECT data,descricao,valor,datainicio,datafinal,numparcelas FROM parcelamentos WHERE datafinal>? ORDER BY data",(hoje,)).fetchall():
            linha=[]
            for el in campo:
                linha.append(el)
            meses_c=conta_meses(campo[3],hoje)        
            valor=float(campo[2])
            soma_total+=valor
            saldo=valor-((valor/campo[5])*meses_c)
            soma_saldo+=saldo
            soma_parcela+=((float(campo[2])/float(campo[5])))
            linha.append('%.2f'%(float(campo[2])/float(campo[5])))
            linha.append('%.2f'%saldo)
            for indice in range(0,len(linha)):linha[indice]=str(linha[indice])
            md.append(linha)
        md.append(['','Totais','%.2f'%soma_total,'','','','%.2f'%soma_parcela,'%.2f'%soma_saldo])
        self.lista.set_model(md)

    def make_toolbar(self):
        tb =Toolbar()
        bts = [ ToolButton(stock_id="gtk-new"), SeparatorToolItem(),ToolButton(stock_id="gtk-delete"),SeparatorToolItem(), ToolButton(stock_id="gtk-close") ]
        bts[0].connect("clicked",lambda evt:self.novo())
        bts[2].connect("clicked",lambda evt:self.apagar())
        bts[4].connect("clicked",lambda evt:self.destroy())
        for bt in bts:tb.add(bt)
        return(tb)
#-----------------------------------------------------------------------------------
class ParcelamentosDLG(Dialog):
    def __init__(self):
        Dialog.__init__(self)
        self.set_title("Parcelamentos")
        self.add_buttons("gtk-ok",1,"gtk-cancel",0)
        bx = HBox(homogeneous=False,spacing=0)
        self.descricao= Entry()
        self.data=DatePicker()
        bx.pack_start(self.make_label("Descrição",self.descricao),True,True,1)
        bx.pack_end(self.make_label("Data",self.data),False,False,1)
        self.vbox.pack_start(bx,False,True,1)
        self.datainicio=DatePicker()
        self.valor=Entry()
        self.numparcelas=Gtk.SpinButton()
        self.numparcelas.set_increments(1,10)
        self.numparcelas.set_range(1,1000)
        bx = HBox(homogeneous=False,spacing=0)
        bx.pack_start(self.make_label("Data primeira parcela",self.datainicio),True,True,1)
        bx.pack_start(self.make_label("Valor",self.valor),True,True,1)
        bx.pack_start(self.make_label("Numero de parcelas",self.numparcelas),False,True,1)
        self.vbox.pack_start(bx,False,True,1)
        self.show_all()

    def make_label(self,lb,entry):
        fr = Frame(label=lb)
        fr.set_shadow_type(Gtk.ShadowType.NONE)
        fr.add(entry)
        return(fr)
#-------------------------------------------------------------------------------------------------------------------------------------------------------        
#
class ListaEventos(Frame):
    def __init__(self,parent):
        self.top=parent
        self.db = parent.db
        Frame.__init__(self)
        self.lista = TreeView()
        self.lista.append_column(TreeViewColumn("",CellRendererText(),text=0))
        self.lista.connect("row-activated",lambda wdg,ix,obj:self.abrir())
        self.lista.set_headers_visible(False)
        base = VBox(homogeneous=False,spacing=0)
        tb = Toolbar()
        bt1 = ToolButton(stock_id="gtk-find");bt1.connect("clicked",lambda evt:self.buscar())
        bt2 = ToolButton(stock_id="gtk-new");bt2.connect("clicked",lambda evt:self.novo())
        bt3 = ToolButton(stock_id="gtk-refresh");bt3.connect("clicked",lambda evt:self.listar())
        bt4 = ToolButton(stock_id="gtk-delete");bt4.connect("clicked",lambda evt:self.apagar())
        bt5 = ToolButton(stock_id="gtk-close");bt5.connect("clicked",lambda evt:self.destroy())
        tb.add(bt1)
        tb.add(bt2)
        tb.add(SeparatorToolItem())
        tb.add(bt4)
        tb.add(SeparatorToolItem())        
        tb.add(bt3)
        tb.add(bt5)
        base.pack_start(self.lista,True,True,1)
        base.pack_end(tb,False,True,1)
        self.add(base)
        self.show_all()
        self.pad=''
        self.listar()
        self.top.desktop.set_current_page(-1)

    def buscar(self):
        dlg=Dialog()
        dlg.add_buttons("gtk-ok",1,"gtk-cancel",0)
        texto = Entry()
        fr=Frame(label="Procurar:")
        fr.add(texto)
        dlg.vbox.pack_start(fr,True,True,1)
        dlg.show_all()
        if dlg.run():
            self.pad=texto.get_text()
            self.listar()
        dlg.destroy()

    def novo(self):
        self.top.desktop.append_page(Eventos(self.top),Label(label="Novo envento"))       
        self.top.desktop.set_current_page(-1)

    def abrir(self):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        descricao = md[it][0]
        self.top.desktop.append_page(Eventos(self.top,descricao),Label(label=descricao))       
        self.top.desktop.set_current_page(-1)

    def listar(self):
        md = ListStore(str)
        if self.pad=='':
            for evt in self.db.execute("SELECT descricao FROM eventos ORDER BY data,descricao").fetchall():
                md.append(evt)
        else:
            for evt in self.db.execute("SELECT descricao FROM eventos WHERE descricao like ? ORDER BY data,descricao",('%%%s%%'%self.pad,)).fetchall():
                md.append(evt)            
        self.lista.set_model(md)        

    def apagar(self):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        descricao = md[it][0]
        dlg=Gtk.MessageDialog(parent=self.top,buttons=Gtk.ButtonsType.YES_NO)
        dlg.set_markup("Apagar o evento %s?\nTodos os lancamento também serão apagados"%descricao)
        ret=dlg.run();dlg.destroy()
        if ret==Gtk.ResponseType.YES:
            self.db.execute("DELETE FROM eventos WHERE descricao=?",(descricao,))
            self.db.execute("DELETE FROM lancamento_eventos WHERE evento=?",(descricao,))
            self.db.commit()
            self.listar()
#----------------------------------------------------------------------------------------------                
# Controle de gastos por evento, permite isolar os gastos de um evento(ex. uma viagem) não
# participa diretamente da projeção.
#
class Eventos(Frame):
    def __init__(self,parent,desc=''):
        self.descricao_original=desc
        self.top=parent
        self.db=parent.db
        Frame.__init__(self)
        tb=Toolbar()
        bt1 = ToolButton(stock_id="gtk-save");bt1.connect("clicked",self.salvar)
        bt2 = ToolButton(stock_id="gtk-close");bt2.connect("clicked",lambda evt:self.destroy())
        tb.add(bt1)
        tb.add(SeparatorToolItem())
        tb.add(bt2)
        base = VBox(spacing=0,homogeneous=False)
        base.pack_end(tb,False,True,0)
        self.descricao=Entry();self.descricao.set_text(self.descricao_original)
        self.data=DatePicker()
        self.lista=TreeView()
        self.lista.append_column(TreeViewColumn("Data",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Descrição",CellRendererText(),text=1))
        self.lista.append_column(TreeViewColumn("Valor",CellRendererText(),text=2))        
        self.obs=Gtk.TextView()
        setattr(self.obs,'get_text',lambda:self.obs.get_buffer().get_text(self.obs.get_buffer().get_start_iter(),self.obs.get_buffer().get_end_iter(),False))
        ln1 = HBox(spacing=0,homogeneous=False)
        base.pack_start(ln1,False,True,1)
        ln1.pack_start(self.make_label(self.descricao,"Descrição:"),True,True,1)
        ln1.pack_start(self.make_label(self.data,"Data:"),False,True,1)
        ln1 = VBox(spacing=0,homogeneous=False)
        fr = Frame(label="Obs.:")
        scw1=ScrolledWindow()        
        scw1.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        scw1.add(self.obs)
        fr.add(scw1)
        base.pack_start(fr,False,True,1)
        fr = Frame(label="Lancamentos:")
        ctn = VBox(spacing=0,homogeneous=False)
        tb = Toolbar()
        tb.set_icon_size(Gtk.IconSize.MENU)
        tb.set_style(Gtk.ToolbarStyle.ICONS)
        bts = [ ToolButton(stock_id="gtk-new"),SeparatorToolItem(), ToolButton(stock_id="gtk-delete")]
        bts[0].connect("clicked",lambda evt:self.novo_lancamento())
        bts[2].connect("clicked",lambda evt:self.apaga_lancamento())        
        for bt in bts:tb.add(bt)      
        ctn.pack_start(tb,False,True,1)
        scw1=ScrolledWindow()
        scw1.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        scw1.add(self.lista)
        ctn.pack_start(scw1,True,True,1)
        fr.add(ctn)
        base.pack_start(fr,True,True,1)
        self.add(base)
        self.show_all()
        if desc.strip()!='':self.abrir(desc)

    def novo_lancamento(self):
        self.salvar(None)
        dlg=DLGLancamentoEvento(self)
        if dlg.run() and self.db.execute("SELECT count(descricao) FROM lancamento_eventos WHERE descricao=? AND data=? AND valor=?",(dlg.descricao.get_text(),dlg.data.get(),dlg.valor.get_text())).fetchone()[0]<=0:
            try:
                self.db.execute("INSERT INTO lancamento_eventos(evento,data,descricao,valor) VALUES(?,?,?,?)",
                    (self.descricao.get_text(),dlg.data.get(),dlg.descricao.get_text(),dlg.valor.get_text()))
                self.db.commit()
                self.lista_lancamentos()
            except Exception as e:
                print(e)
        dlg.destroy()            

    def apaga_lancamento(self):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        data = md[it][0]
        descricao =md[it][1]
        valor =md[it][2]
        dlg = Gtk.MessageDialog(parent=self.top,buttons=Gtk.ButtonsType.YES_NO)
        dlg.set_markup("Apagar o lancamento:\n%s\n%s\n%s"%(data,descricao,valor))
        if dlg.run()==Gtk.ResponseType.YES:
            self.db.execute("DELETE FROM lancamento_eventos WHERE evento=? AND data=? AND descricao=? AND valor=?",(self.descricao_original,data,descricao,valor))
            self.db.commit()
            self.lista_lancamentos()
        dlg.destroy()

    def abrir(self,desc):
        descricao,data,obs = self.db.execute("SELECT descricao,data,obs FROM eventos WHERE descricao=?",(desc,)).fetchone()
        self.data.set(data)
        self.descricao.set_text(descricao)
        self.obs.get_buffer().set_text(obs)
        self.descricao_original=desc
        self.lista_lancamentos()

    def salvar(self,evt):
        if self.descricao.get_text().strip()=='':return
        self.top.desktop.set_tab_label_text(self,self.descricao.get_text())        
        if self.db.execute("SELECT count(descricao) FROM eventos WHERE descricao=?",(self.descricao.get_text().strip(),)).fetchone()[0]<=0:
            self.db.execute("INSERT INTO eventos(descricao,data,obs) VALUES(?,?,?)",(self.descricao.get_text().strip(),self.data.get(),self.obs.get_text()))
        else:
            self.db.execute("UPDATE eventos SET descricao=?,data=?,obs=? WHERE descricao=? ",(self.descricao.get_text().strip(),self.data.get(),self.obs.get_text(),self.descricao_original))
            self.db.execute("UPDATE lancamento_eventos SET evento=? WHERE evento=?",(self.descricao.get_text().strip(),self.descricao_original))
            self.descricao_original=self.descricao.get_text().strip()
            self.lista_lancamentos()
        self.db.commit()
        
    def lista_lancamentos(self):        
        md = ListStore(str,str,str)
        for lcm in self.db.execute("SELECT data,descricao,valor FROM lancamento_eventos WHERE evento=? ORDER BY data,descricao",(self.descricao.get_text(),)).fetchall():
            linha = [lcm[0],lcm[1],'%.2f'%float(lcm[2])]
            md.append(linha)
        for lcm in self.db.execute("SELECT '','Valor total:',sum(valor) FROM lancamento_eventos WHERE evento=?",(self.descricao.get_text(),)).fetchall():
            if not(lcm[2]):
                    linha = [lcm[0],lcm[1],'0.00']        
            else: linha = [lcm[0],lcm[1],'%.2f'%float(lcm[2])]
            md.append(linha)
        self.lista.set_model(md)

    def make_label(self,entry,lb):
        fr = Frame(label=lb)
        fr.set_shadow_type(Gtk.ShadowType.NONE)
        fr.add(entry)
        return(fr)

class DLGLancamentoEvento(Dialog):
    def __init__(self,top):
        Dialog.__init__(self)
        self.descricao=Entry()
        self.vbox.pack_start(top.make_label(self.descricao,"Descrição:"),False,True,1)
        self.data=DatePicker()
        self.vbox.pack_start(top.make_label(self.data,"Data:"),False,True,1)
        self.valor=Entry()
        self.vbox.pack_start(top.make_label(self.valor,"Valor:"),False,True,1)
        self.add_buttons("gtk-ok",1,"gtk-cancel",0)
        self.show_all()
    
#-------------------------------------------------------------------------------------------------------------------------------------------------------        
# Controle de gastos recorrentes - gastos que ocorrem todo mês mas não tem valor fixo ex.:Agua,luz,telefone
# A intenção é lancar os valores médios apurados na projeção de gastos 
#
class GastosRecorrentes(Frame):
    def __init__(self,parent):
        Frame.__init__(self)
        self.top=parent
        bx = VBox(homogeneous=False,spacing=0)
        bx.pack_start(self.make_toolbar(),False,True,1)
        self.lista = TreeView()
        scw = ScrolledWindow()
        scw.add(self.lista)
        scw.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        bx.pack_start(scw,True,True,1)
        self.db = parent.db      
        self.add(bx)
        self.atualizar()
        self.show_all()


    def atualizar(self):
        for c in self.lista.get_columns():self.lista.remove_column(c)
        meses = ['','Janeiro  ','Fevereiro','Março','Abril','Maio','Junho','Julho','Agosto','Setembro','Outubro','Novembro','Dezembro']
        dia,mes,ano = list(map(int,time.strftime("%d-%m-%Y").split("-")))
        controle = []
        if mes==12:
            mes=1
        else:mes+=1
        ano-=1
        anoini=ano # Limite o calculo do consumo médio ao consumo dos ultimos 12 meses 
        self.lista.append_column(TreeViewColumn("Empresa      ",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Média   ",CellRendererText(),text=1))
        for i in range(0,12):
            self.lista.append_column(TreeViewColumn(meses[mes].zfill(9).replace('0',' ')+'\n'+str(ano),CellRendererText(),text=i+2))
            controle.append((mes,ano))
            mes+=1
            if mes>12:
                mes=1
                ano+=1
        md = ListStore(str,str,str,str,str,str,str,str,str,str,str,str,str,str)
        self.lista.set_model(md)
        for empresa in self.db.execute("SELECT descricao FROM recorrente GROUP BY descricao").fetchall():
            empresa=empresa[0]
            media = '%.2f'%self.db.execute("SELECT (SUM(valor)/COUNT(descricao)) FROM recorrente WHERE descricao=? AND ano>=?",(empresa,anoini)).fetchone()[0]
            linha = [ empresa , media]
            for data in controle:
                valor = self.db.execute("SELECT valor FROM recorrente WHERE mes=? AND ano=? AND descricao=?",(data[0],data[1],empresa)).fetchone()
                if valor==None:valor=0
                linha.append('%.2f'%valor)
            md.append(linha)

    def make_toolbar(self):
        tb = Toolbar()
        bts = [ToolButton(stock_id="gtk-new"),ToolButton(stock_id="gtk-delete"),SeparatorToolItem(),ToolButton(stock_id="gtk-close")]
        bts[0].connect("clicked",lambda evt:self.novo())
        bts[1].connect("clicked",lambda evt:self.apagar())
        bts[3].connect("clicked",lambda evt:self.destroy())
        for bt in bts:tb.add(bt)
        return(tb)

    def novo(self):
        dlg = DLGRecorrente(self.top)
        mes,ano = list(map(int,time.strftime('%m-%Y').split('-')))
        dlg.ano.set_value(ano)
        dlg.mes.set_value(mes)
        dlg.empresa.grab_focus()
        if dlg.run():
            contador = self.db.execute("SELECT COUNT(descricao) FROM recorrente WHERE descricao=? AND mes=? AND ano=?",(dlg.empresa.get_text(),
            '%.2i'%dlg.mes.get_value(),dlg.ano.get_value())).fetchone()[0]
            if contador<=0:
                self.db.execute("INSERT INTO recorrente(descricao,valor,mes,ano) VALUES(?,?,?,?)",(dlg.empresa.get_text(),
                            dlg.valor.get_text(),'%.2i'%dlg.mes.get_value(),dlg.ano.get_value()))
            else:
                self.db.execute("UPDATE recorrente SET valor=? WHERE descricao=? AND mes=? AND ano=?",(dlg.valor.get_text(),
                            dlg.empresa.get_text(),'%.2i'%dlg.mes.get_value(),dlg.ano.get_value()))
            self.db.commit()
            self.atualizar()
        dlg.destroy()

    def apagar(self):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        empresa=md[it][0]
        dlg=DLGApagarRecorrente(self.top)
        dlg.empresa.set_text(empresa)
        if dlg.run():
            self.db.execute("UPDATE recorrente SET valor=0 WHERE descricao=? AND mes=? AND ano=?",(empresa,dlg.mes.get_value(),dlg.ano.get_value()))
            self.db.commit()
            self.atualizar()
        dlg.destroy()
    
class DLGApagarRecorrente(Dialog):
    def __init__(self,parent):
        Dialog.__init__(self,parent=parent)
        mes,ano = list(map(int,time.strftime('%m-%Y').split('-')))
        fr = []
        fr.append(Frame(label="Mês:"))
        self.mes = Gtk.SpinButton()
        self.mes.set_increments(1,1)
        self.mes.set_range(1,12)
        self.mes.set_value(mes)
        fr[0].add(self.mes)
        fr.append(Frame(label="Ano:"))
        self.ano=Gtk.SpinButton()
        self.ano.set_increments(1,10)
        self.ano.set_range(1,9999)
        self.ano.set_value(ano)
        fr[1].add(self.ano)
        fr.append(Frame(label="Empresa:"))
        self.empresa=Entry()
        self.empresa.set_property('editable',False)
        fr[2].add(self.empresa)
        for f in fr:self.vbox.pack_start(f,False,True,1)
        self.add_buttons("gtk-ok",1,"gtk-cancel",0)
        self.show_all()
        
class DLGRecorrente(Dialog):
    def __init__(self,parent):
        Dialog.__init__(self,parent=parent)
        fr = []
        fr.append(Frame(label="Mês:"))
        self.mes = Gtk.SpinButton()
        self.mes.set_increments(1,1)
        self.mes.set_range(1,12)
        fr[0].add(self.mes)
        fr.append(Frame(label="Ano:"))
        self.ano=Gtk.SpinButton()
        self.ano.set_increments(1,10)
        self.ano.set_range(1,9999)
        fr[1].add(self.ano)
        fr.append(Frame(label="Empresa:"))
        self.empresa=Entry()
        fr[2].add(self.empresa)
        fr.append(Frame(label="Valor:"))
        self.valor=Entry()
        fr[3].add(self.valor)
        for f in fr:self.vbox.pack_start(f,False,True,1)
        self.add_buttons("gtk-ok",1,"gtk-cancel",0)
        self.show_all()
        
#-------------------------------------------------------------------------------------------------------------------------------------------------------        
# Controle dos gastos do mes/dividida em dois objetos, um generico e outro especifico para cartões
# a BaseGastos é o objeto base para as duas
class BaseGastos(Frame):
    def __init__(self,parent):
        Frame.__init__(self)
        self.my_accelerators = AccelGroup()        
        bx  = VBox(homogeneous=False,spacing=0)
        self.top = parent
        self.db=self.top.db
        self.lista=TreeView()
        if self.__class__.__name__.lower()!='fatura':
            self.lista.append_column(TreeViewColumn("D/C",Renderer(),text=0,foreground=5))
            self.lista.append_column(TreeViewColumn("Data",Renderer(),text=1,foreground=5))
            self.lista.append_column(TreeViewColumn("Descrição",Renderer(),text=2,foreground=5))
            self.lista.append_column(TreeViewColumn("Tipo",Renderer(),text=3,foreground=5))
            self.lista.append_column(TreeViewColumn("Valor",Renderer(),text=4,foreground=5))
            self.lista.set_model(ListStore(str,str,str,str,str,str))
            self.lista.get_column(3).set_min_width(100)
            self.lista.get_column(1).set_min_width(100)
        else:
            self.lista.append_column(TreeViewColumn("Data",Renderer(),text=0,foreground=3))
            self.lista.append_column(TreeViewColumn("Descrição",Renderer(),text=1,foreground=3))
            self.lista.append_column(TreeViewColumn("Valor",Renderer(),text=2,foreground=3))
            self.lista.get_column(1).set_min_width(400)
        self.lista.get_column(0).set_min_width(50)
        self.lista.get_column(2).set_min_width(300)
        self.lista.connect("row-activated",lambda evt,md,idx:self.abre_lancamento())
        self.lista.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        bx.pack_start(self.make_toolbar(),False,True,1)
        if self.__class__.__name__.lower()!='fatura':
            self.datai = DatePicker("Data inicial:")
            self.dataf = DatePicker("Data final:")
            bxdata = HBox(homogeneous=False,spacing=0)
            bxdata.pack_start(self.datai,True,True,1)
            bxdata.pack_start(self.dataf,True,True,1)
            btdata = ToolButton(stock_id="gtk-refresh")
            btdata.connect("clicked",lambda evt:self.atualizar())
            bxdata.pack_end(btdata,False,False,1)            
            bx.pack_end(bxdata,False,True,1)
        scw = ScrolledWindow()
        scw.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        scw.add(self.lista)        
        bx.pack_end(scw,True,True,0)
        self.add(bx)
        if self.__class__.__name__.lower()!='fatura':
            data2=time.strftime("%Y-%m-%d")
            ano,mes,dia = list(map(int,data2.split("-")))
            if mes==12:
                mes=1
            else:mes-=1
            data1 = '%i-%.2i-%.2i'%(ano,mes,dia)
            self.datai.set(data1)
            self.dataf.set(data2)
        self.show_all() 
        self.atualizar()
    
    def make_toolbar(self):
        tb = Toolbar()
        tb.set_style(Gtk.ToolbarStyle.BOTH)
        bts = [ToolButton(stock_id="gtk-remove"),
               ToolButton(stock_id="gtk-add"),
               SeparatorToolItem(),
               ToolButton(stock_id="gtk-delete"),
               SeparatorToolItem(),
               ToolButton(stock_id="gtk-close"),
               SeparatorToolItem()]
        bts[0].set_label("Débito")
        bts[1].set_label("Crédito")
        #self.add_accelerator(bts[0], "<Control>c", signal="clicked")
        bts[0].connect("clicked",lambda evt:self.lancamento("debito"))
        bts[1].connect("clicked",lambda evt:self.lancamento("credito"))
        bts[3].connect("clicked",lambda evt:self.apaga_lancamento())
        bts[5].connect("clicked",lambda evt:self.destroy())
        for bt in bts:tb.add(bt) 
        return(tb)

    def atualizar(self):
        saldo =0
        md=None
        if self.__class__.__name__.lower()!='fatura':
            md=ListStore(str,str,str,str,str,str)    
            data1 = self.datai.get()
            data2 = self.dataf.get()            
            for lanc in self.db.execute("SELECT data,descricao,tipo,valor FROM lancamento WHERE data>=? AND data<=? ORDER BY data",(data1,data2)).fetchall():
                linha = ["",self.formata_data(lanc[0]),lanc[1],lanc[2],'%.2f'%float(lanc[3]),""]
                if lanc[3]==None:return
                if float(lanc[3])<=0:
                    linha[5] = "#FF0000"
                    linha[0] = "D"	
                else:
                    linha[5] = "#0FF000"
                    linha[0] = "C"
                md.append(linha)
                saldo+=float(lanc[3])
            md.append(["","","Saldo","",'%.2f'%float(saldo),"#000000"]) 
        else:
            md=ListStore(str,str,str,str)     
            for lanc in self.db.execute("SELECT data,descricao,valor,'#000000' FROM fatura WHERE aberta='1'").fetchall():
                linha = [self.formata_data(lanc[0]),lanc[1],'%.2f'%float(lanc[2]),lanc[3]]
                md.append(linha)
            try:
                saldo=float(self.db.execute("SELECT SUM(valor) FROM fatura WHERE aberta='1'").fetchone()[0])
            except:saldo=0.00
            md.append(["","Saldo",'%.2f'%saldo,"#000000"])
        self.lista.set_model(md)

    def add_accelerator(self, widget, accelerator, signal="clicked"):
        """Adds a keyboard shortcut"""
        if accelerator is not None:
            key, mod = accelerator_parse(accelerator)
            widget.add_accelerator(signal, self.my_accelerators, key, mod, ACCEL_VISIBLE)

    def formata_data(self,data):
         ano,mes,dia =data.split("-")
         return(dia+"/"+mes+"/"+ano)

    def lancamento(self,tp='cc'): 
        dlg=DLGLancamento(self.top,tp)
        if dlg.run():
            if tp=='debito':
                valor = float(dlg.valor.get_text())*-1
            else:
                valor=float(dlg.valor.get_text())
            tpos=[]
            if tp=='cc': #cc==cartao credito
                sql="INSERT INTO fatura(data,descricao,valor,aberta) VALUES(?,?,?,'1')"
                valores=(dlg.data.get(),dlg.descricao.get_text(),valor)
            else:
                sql="INSERT INTO lancamento(data,descricao,valor,tipo) VALUES(?,?,?,?)"
                valores=(dlg.data.get(),dlg.descricao.get_text(),valor,dlg.tipo.get_model()[dlg.tipo.get_active()][0])
            try:
                self.db.execute(sql,valores)
                self.db.commit()
            except Exception as e:
               dl = MessageDialog(parent=self.top,buttons=BUTTONS_OK)
               dl.set_markup(str(e))
               dl.run();dl.destroy()
        dlg.destroy()        
        self.atualizar()

    def abre_lancamento(self): 
            md,idx = self.lista.get_selection().get_selected_rows()
            if len(idx)==0:return
            idx=idx[0]
            descricao=''
            sinal='cc'
            if self.__class__.__name__.lower()=='fatura':
                tipo='cc'
                dia,mes,ano = md[idx][0].split('/')
                data=ano+'-'+mes+'-'+dia
                descricao=md[idx][1]
            else:
                sinal = {'d':'debito','c':'credito'}[md[idx][0].lower()]
                dia,mes,ano = md[idx][1].split('/')
                data=ano+'-'+mes+'-'+dia
                descricao=md[idx][2]
            dlg = DLGLancamento(self.top,sinal)
            dlg.data.set(data)
            dlg.descricao.set_text(descricao)            
            indx = 0
            if tipo!='cc':
                for x in dlg.tipo.get_model():            
                    if x[0]==md[idx][3]:
                        dlg.tipo.set_active(indx)
                    else:indx+=1
            if sinal=='debito':
                dlg.valor.set_text('%.2f'%(float(md[idx][4])*-1))                
            elif sinal=='credito':
                    dlg.valor.set_text(md[idx][4])  
            else:dlg.valor.set_text(md[idx][2])
            try:
                linha = md[idx]
                if dlg.run():
                    if sinal=='debito' and not(sinal=='cc'):
                        valor = float(dlg.valor.get_text()) * -1
                        dia,mes,ano = linha[1].split('/')
                        linha[1]=ano+'-'+mes+'-'+dia                    
                    else:
                        valor = float(dlg.valor.get_text())
                        dia,mes,ano = linha[0].split('/')
                        linha[0]=ano+'-'+mes+'-'+dia                    
                    if sinal!='cc':
                        sql="UPDATE lancamento SET data=?,descricao=?,valor=?,tipo=? WHERE data=? AND descricao=? AND valor=? AND tipo=?"
                        valores=(dlg.data.get(),dlg.descricao.get_text(),
                                 valor,dlg.tipo.get_model()[dlg.tipo.get_active()][0],linha[1],linha[2],linha[4],linha[3])
                    else:
                        sql="UPDATE fatura SET data=?,descricao=?,valor=? WHERE data=? AND descricao=? AND valor=?"
                        valores=(dlg.data.get(),dlg.descricao.get_text(),valor,linha[0],linha[1],linha[2])           
                    self.db.execute(sql,valores)
                    self.db.commit()
                    self.atualizar()
            except Exception as e:
                    dl = MessageDialog(parent=self.top,buttons=BUTTONS_OK)
                    dl.set_markup(str(e))
                    dl.run();dl.destroy()
            dlg.destroy()
    
    def apaga_lancamento(self):
            md,idx = self.lista.get_selection().get_selected_rows()
            if len(idx)==0:return
            dlg = Gtk.MessageDialog(parent=self.top,buttons=Gtk.ButtonsType.YES_NO)
            dlg.set_markup("Apagar os lancamentos selecionado")
            if dlg.run()==Gtk.ResponseType.YES:  
                for item in idx:
                    linha = md[item]
                    if self.__class__.__name__.lower()=='fatura':
                        dia,mes,ano = linha[0].split('/')
                        linha[0]=ano+'-'+mes+'-'+dia
                        self.db.execute("DELETE FROM fatura WHERE data=? AND descricao=?  AND valor=?",(linha[0],linha[1],linha[2]))            
                    else:                
                        dia,mes,ano = linha[1].split('/')
                        linha[1]=ano+'-'+mes+'-'+dia
                        self.db.execute("DELETE FROM lancamento WHERE data=? AND descricao=? AND tipo=? AND valor=?",(linha[1],linha[2],linha[3],linha[4]))
                self.db.commit()
                self.atualizar()
            dlg.destroy()

#
class Gastos(BaseGastos):pass        
class Fatura(BaseGastos):
    def make_toolbar(self):
        tb = Toolbar()
        img=Image()
        img.set_from_pixbuf(Pixbuf.new_from_xpm_data(importar_xpm))
        tb.set_style(Gtk.ToolbarStyle.BOTH)
        bts = [ToolButton(stock_id="gtk-execute"),
               SeparatorToolItem(),
               ToolButton(stock_id="gtk-delete"),
               SeparatorToolItem(),
               ToolButton(stock_id="gtk-yes"),
               SeparatorToolItem(),
               ToolButton(stock_id="gtk-close"),
               SeparatorToolItem(),
               ToolButton(icon_widget=img)]
        bts[0].set_label("Lancamentos")
        bts[4].set_label("Fechar fatura")
        bts[8].set_label("Importar ofx")
        #self.add_accelerator(bts[0], "<Control>c", signal="clicked")
        bts[0].connect("clicked",lambda evt:self.lancamento())
        bts[2].connect("clicked",lambda evt:self.apaga_lancamento())
        bts[4].connect("clicked",lambda evt:self.fechar_fatura())
        bts[6].connect("clicked",lambda evt:self.destroy())
        bts[8].connect("clicked",lambda evt:self.importar_ofx())
        for bt in bts:tb.add(bt) 
        return(tb)

    def importar_ofx(self):
        dlg=DLGImport(self)
        if dlg.run():
            md=dlg.lista.get_model()
            it = md.get_iter_first()
            while True:
                try:                    
                    data=md.get(it,0)[0].split('/')
                    data=data[2]+'-'+data[1]+'-'+data[0]
                    linha = [data,md.get(it,1)[0],md.get(it,2)[0]]
                    contador=self.db.execute(
                    "SELECT COUNT(descricao) FROM fatura WHERE data=? AND descricao=? AND valor=?",linha).fetchone()[0]
                    if contador<=0:
                        self.db.execute("INSERT INTO fatura(data,descricao,valor) VALUES(?,?,?)",linha)            
                    it=md.iter_next(it)
                except:break
            self.db.commit()        
            self.atualizar()
        dlg.destroy()

    def fechar_fatura(self):
        dlg=Gtk.MessageDialog(parent=self.top,buttons=Gtk.ButtonsType.YES_NO)
        dlg.set_markup("Fechar fatura?")
        if dlg.run()==Gtk.ResponseType.YES:
            self.db.execute("UPDATE fatura SET aberta='0' WHERE aberta='1'")
            self.db.commit()
            self.atualizar()
        dlg.destroy()
#
#-------------------------------------------------------------------------------------------------------------------------------------------------------        
#
class TiposLancamentos(Window):
    def __init__(self,top):
        self.db=top.db
        Window.__init__(self)
        bx  = VBox(homogeneous=False,spacing=0)
        tb = Toolbar()
        bt1 = ToolButton(stock_id="gtk-close");tb.add(bt1)
        tb.add(SeparatorToolItem())
        bt2 = ToolButton(stock_id="gtk-new");tb.add(bt2)
        bt3 = ToolButton(stock_id="gtk-delete");tb.add(bt3)
        bt1.connect("clicked",lambda evt:self.destroy())
        bt2.connect("clicked",lambda evt:self.novo())
        bt3.connect("clicked",lambda evt:self.apagar())
        bx.pack_start(tb,False,True,1)
        self.lista = TreeView()
        scw = ScrolledWindow()
        scw.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        scw.add(self.lista)
        bx.pack_end(scw,True,True,0)
        self.add(bx)
        self.lista.append_column(TreeViewColumn("D/C",CellRendererText(),text=0))
        self.lista.set_headers_visible(False)
        self.atualizar()
        self.set_size_request(300,400)
        self.set_title("Tipos de lancamentos")
        self.show_all()

    def novo(self):
        dlg = Dialog()
        dlg.add_buttons("gtk-ok",1,"gtk-cancel",0)
        fr = Frame(label="Descrição:")
        setattr(dlg,"descricao",Gtk.Entry())
        fr.add(dlg.descricao)
        dlg.vbox.add(fr)
        dlg.show_all()
        if dlg.run():
            ct =self.db.execute("SELECT count(descricao) FROM tiposlancamento WHERE descricao=?",(dlg.descricao.get_text(),)).fetchone()[0]
            if ct==0:
                self.db.execute("INSERT INTO tiposlancamento VALUES(?)",(dlg.descricao.get_text(),))
                self.db.commit()
                self.atualizar()
        dlg.destroy()

    def atualizar(self):
        md = ListStore(str)
        for tp  in self.db.execute("SELECT descricao FROM tiposlancamento ORDER BY descricao").fetchall():md.append(tp)        
        self.lista.set_model(md)

    def apagar(self):
        md,it = self.lista.get_selection().get_selected()
        if it==None:return
        descricao = md.get_value(it,0)
        dlg=Gtk.MessageDialog(parent=self,message_type=Gtk.MessageType.INFO,buttons=Gtk.ButtonsType.YES_NO)
        dlg.set_markup("Apagar:%s"%descricao)
        if dlg.run()==Gtk.ResponseType.YES:
            self.db.execute("DELETE FROM tiposlancamento WHERE descricao=?",(descricao,))
            self.db.commit()
        dlg.destroy()
        self.atualizar()
#-------------------------------------------------------------------------------------------------------------------------------------------------------
# Exportador de dados, lê as tabelas e campos do SQLITE_MASTER e exporta as tabelas seleciondas como json
#
class ExportarLancamentos(Window):
    def __init__(self,parent):
        self.db = parent.db
        Window.__init__(self)
        self.set_size_request(640,480)
        self.set_title("Exportar")
        base = VBox(homogeneous=False,spacing=0)
        fr=Frame(label="Destino:")
        b=HBox(homogeneous=False,spacing=0)
        fr.add(b)
        self.destino=Entry();b.pack_start(self.destino,True,True,1)
        bt = ToolButton(stock_id="gtk-file");bt.connect("clicked",lambda evt:self.seleciona_saida())
        b.pack_end(bt,False,False,0)
        base.pack_start(fr,False,True,0)
        self.tabelas = TreeView()
        self.tabelas.append_column(TreeViewColumn("Tabela",CellRendererText(),text=0))
        scw=ScrolledWindow()
        scw.add(self.tabelas)
        base.pack_start(scw,True,True,0)
        btb=HBox(homogeneous=True,spacing=0)
        bt1 = ToolButton(stock_id="gtk-ok");bt1.connect("clicked",lambda evt:self.ok())
        bt2 = ToolButton(stock_id="gtk-cancel");bt2.connect("clicked",lambda evt:self.destroy())
        btb.pack_start(bt1,True,True,0)
        btb.pack_start(bt2,True,True,0)
        base.pack_end(btb,False,False,0)
        self.add(base)
        md=ListStore(str)
        self.tabelas.get_selection().set_mode(Gtk.SelectionMode.MULTIPLE)
        for n in self.db.execute("SELECT name FROM SQLITE_MASTER WHERE type='table'").fetchall():md.append(n)
        self.tabelas.set_model(md)

    def ok(self):
        fname=self.destino.get_text()        
        if fname.strip()=='':return        
        (md,lista)=self.tabelas.get_selection().get_selected_rows()        
        if len(lista)<=0:
            dlg=Gtk.MessageDialog(buttons=Gtk.ButtonsType.OK,message_type=Gtk.MessageType.ERROR);dlg.set_markup("Escolha pelo menos uma tabela")
            dlg.run();dlg.destroy()
            return
        saida={}
        encoder=json.encoder.JSONEncoder()
        for t in lista:
            tabela=md[t][0]
            saida[tabela]=self.sql2dict(tabela)
        try:
            dlg=Gtk.MessageDialog(parent=self,buttons=Gtk.ButtonsType.OK,message_type=Gtk.MessageType.INFO,text="Dados gravados")
            fsaida=open(fname,'wt')
            fsaida.write(encoder.encode(saida))
            fsaida.flush()
            fsaida.close()
            dlg.run();dlg.destroy()
        except Exception as e:
            dlg=Gtk.MessageDialog(parent=self,buttons=Gtk.ButtonsType.OK,message_type=Gtk.MessageType.ERROR,text="ERRO gravando saida\n%s"%str(e))
            dlg.run();dlg.destroy()            

    def seleciona_saida(self):
        df = Gtk.FileChooserDialog(action=Gtk.FileChooserAction.SAVE,title="Exportar")
        df.add_buttons("gtk-ok",1,"gtk-cancel",0)
        ft = Gtk.FileFilter()
        ft.set_name("JSON")
        ft.add_pattern("*.json")
        ft2 = Gtk.FileFilter()
        ft2.set_name("Todos os arquivos")
        ft2.add_pattern("*.*")
        df.add_filter(ft);
        df.add_filter(ft2);
        if df.run() and df.get_filename():
            if df.get_filename()[-4:]!='json':
                self.destino.set_text(df.get_filename()+'.json')
            else: self.destino.set_text(df.get_filename())
        df.destroy()       

    def sql2dict(self,tabela):        
        sql_cmd_table="SELECT sql FROM SQLITE_MASTER WHERE name=?"
        sql=self.db.execute(sql_cmd_table,(tabela,)).fetchone()[0].lower().replace("create table "+tabela.lower(),'')
        campos=[]
        for c in sql.replace("(","").split(","):campos.append(c.split(" ")[0])#seleciona os nomes dos campos
        lista_sql=""
        for c in campos:
           if c.strip()!='':lista_sql+=c+","
           if c.strip()=='':campos.pop(campos.index(c))
        lista_sql=lista_sql[:-1]
        sql_cmd="SELECT %s FROM %s"%(lista_sql,tabela)
        valores=[]
        for campo in self.db.execute(sql_cmd).fetchall():
            valor={}
            for ind in range(0,len(campo)):
                valor[campos[ind]]=campo[ind]
            valores.append(valor)
        return(valores)
#-------------------------------------------------------------------------------------------------------------------------------------------------------
#
#
class ImportarLancamentos:
    def __init__(self,parent):
        self.db=parent.db
        self.parent=parent
        df = Gtk.FileChooserDialog(action=Gtk.FileChooserAction.SAVE,title="Exportar")
        df.add_buttons=("gtk-ok",1,"gtk-cancel",0)
        ft = Gtk.FileFilter()
        ft.set_name("JSON")
        ft.add_pattern("*.json")
        ft2 = Gtk.FileFilter()
        ft2.set_name("Todos os arquivos")
        ft2.add_pattern("*.*")
        df.add_filter(ft);
        df.add_filter(ft2);
        if df.run():
            destino=''
            if df.get_filename()[-4:]!='json':
                destino=df.get_filename()+'.json'
            else:destino=df.get_filename()
            self.importar(destino)
        df.destroy()       
        
    def importar(self,arquivo):
        dec=json.decoder.JSONDecoder()
        contador_novos=0
        contador_existente=0
        try:
            dados=dec.decode(open(arquivo).read())
            for k in list(dados.keys()):
                for campo in dados[k]:
                    lista=list(campo.keys())
                    sql,valores=self.monta_contador_sql(k,list(campo.keys()),campo)
                    if self.db.execute(sql,valores).fetchone()[0]>=0:
                        contador_novos+=1
                        sql,param=self.monta_sql_salva(k,list(campo.keys()),campo)
                        self.db.execute(sql,param)
                    else:
                        contador_existente+=1
            message='%.2i Registros inseridos\n%.2i registros já existentes no banco de dados'%(contador_novos,contador_existente)
            dlg=MessageDialog(buttons=BUTTONS_OK,message_format=message);dlg.run()
            dlg.destroy()
            self.db.commit()
        except Exception as e:
            dlg=MessageDialog(buttons=BUTTONS_OK,type=MESSAGE_ERROR,message_format='Erro na abertura de arquivo %s'%str(e))
            dlg.run();dlg.destroy()

    def monta_sql_salva(self,tabela,campos,valores):
        valor=[]
        sql='INSERT INTO '+tabela+'('
        inte=''
        for c in campos:
            sql+=c+','
            inte+='?,'
            valor.append(valores[c])
        sql=sql[:-1]+') VALUES ('+inte[:-1]+')'
        return(str(sql),valor)

    def monta_contador_sql(self,tabela,campos,valores):
        valor=[]
        sql='SELECT '
        sql+='count(%s) FROM %s'%(campos[0],tabela)+' WHERE '
        for c in campos:
            sql+=c+'=? AND '
            valor.append(valores[c])
        sql=sql[:-4]
        return(sql,valor)
            
#-------------------------------------------------------------------------------------------------------------------------------------------------------
#
#
class DLGLancamento(Dialog):
    def __init__(self,parent,tipo='debito'):
        descricao={"debito":"Débito","credito":"Crédito",'cc':'Cartão'}
        Dialog.__init__(self,title="Lancamento %s"%descricao[tipo],parent=parent)
        self.add_buttons("gtk-ok",1,"gtk-cancel",0)
        fr = [ Frame(label="Data:"),Frame(label="Descrição:"),Frame(label="Tipo:"),Frame(label="Valor:")]
        self.data=DatePicker();fr[0].add(self.data)
        self.descricao=Entry();fr[1].add(self.descricao)
        if tipo!='cc':
            self.tipo=Gtk.ComboBoxText();fr[2].add(self.tipo)
            for tp  in parent.db.execute("SELECT descricao FROM tiposlancamento ORDER BY descricao").fetchall():self.tipo.append_text(tp[0])
            self.tipo.append_text('')
            self.tipo.set_active(0)
        self.valor=Entry();fr[3].add(self.valor)
        for f in fr:
            if not(tipo=='cc' and f.get_label()=="Tipo:"):
                self.vbox.pack_start(f,False,True,1)
        self.set_size_request(400,250)
        self.show_all()
#-------------------------------------------------------------------------------------------------------------------------------------------------------
# DLG Import - lista movimento em arquivo OFX antes de importar
#
class DLGImport(Dialog):
    def __init__(self,parent):
        Dialog.__init__(self)
        self.set_size_request(640,480)
        self.lista=TreeView()
        self.lista.append_column(TreeViewColumn("Data",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Valor",CellRendererText(),text=1))
        self.lista.append_column(TreeViewColumn("Descricao",CellRendererText(),text=2))
        self.lista.connect("key-press-event",self.kp)
        md=ListStore(str,str,str);self.lista.set_model(md)
        bx = HBox(homogeneous=False,spacing=False)
        self.path=Entry()
        self.path.set_property('editable',False)
        bt = ToolButton(stock_id="gtk-open")#,ToolButton('gtk-execute')]
        bx.pack_start(self.path,True,True,1)
        bx.pack_end(bt,False,False,1)
        bt.connect("clicked",lambda evt=None:self.abrir())
        self.vbox.pack_start(bx,False,True,1)
        scw=ScrolledWindow()
        scw.set_policy(Gtk.PolicyType.AUTOMATIC,Gtk.PolicyType.AUTOMATIC)
        scw.add(self.lista)
        self.vbox.pack_end(scw,True,True,0)
        self.show_all()
        self.add_buttons('gtk-ok',1,'gtk-cancel',0)

    def kp(self,*args):
        if args[1].hardware_keycode==107: #delete
            md,it = self.lista.get_selection().get_selected()
            if it==None:return
            md.remove(it)

    def abrir(self):
        df = Gtk.FileChooserDialog(action=Gtk.FileChooserAction.OPEN,title="Importar lancamentos")
        df.add_buttons("gtk-ok",1,"gtk-cancel",0)
        ft = Gtk.FileFilter()
        ft.set_name("OFX")
        ft.add_pattern("*.ofx")
        df.add_filter(ft);
        if df.run():
            self.path.set_text(df.get_filename())
            if os.access(df.get_filename(),os.R_OK):
                dados=open(df.get_filename()).read()
                dados=ofxParser(dados).dados
                md=ListStore(str,str,str)
                for campo in dados:md.append([campo['data'],campo['descricao'],campo['valor']])
            self.lista.set_model(md)                
        df.destroy()
    
#-------------------------------------------------------------------------------------------------------------------------------------------------------
#
#
class Renderer(CellRendererText):
    def __init__(self):
        CellRendererText.__init__(self)
        self.set_property('foreground-set',True)        
#-------------------------------------------------------------------------------------------------------------------------------------------------------
#
#
#
class DatePicker(Frame):
    def __init__(self,text=''):
        if text=='':
            Frame.__init__(self)
        else:Frame.__init__(self,label=text)
        bx = HBox(homogeneous=False,spacing=0)
        self.__et =Entry()
        self.__et.set_editable(False)
        image = Image()
        image.set_from_pixbuf(Pixbuf.new_from_xpm_data(["16 16 9 1",
                    " 	c None",
                    ".	c #000000",
                    "+	c #E012EA",
                    "@	c #8F599F",
                    "#	c #FFFFFF",
                    "$	c #9A2424",
                    "%	c #45C174",
                    "&	c #F9F609",
                    "*	c #48CFDB",
                    "                ",
                    " .............. ",
                    " .+++++++++++@. ",
                    " .@@@@@@@@@@@@. ",
                    " .............. ",
                    " .############. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .############. ",
                    " .............. ",
                    "                "]))
        bt =ToolButton(icon_widget=image)
        bt.connect("clicked",self.__set_date)
        bx.pack_start(self.__et,True,True,1)
        bx.pack_end(bt,False,False,0)
        self.add(bx)
        self.__et.set_text(time.strftime("%d/%m/%Y"))
        self.set_shadow_type(Gtk.ShadowType.NONE)

    def set(self,data):
        ano,mes,dia = data.split('-')
        data = '%s/%s/%s'%(dia,mes,ano)
        self.__et.set_text(data)

    def get(self):
        return(self.get_date('y-m-d'))

    def get_date(self,format="d/m/y"):
        '''possible date formats d/m/y y-m-d '''
        dia,mes,ano=self.__et.get_text().split("/")
        return(format.replace("d",dia).replace("m",mes).replace("y",ano))
                
    def __set_date(self,evt):
        dlg=Dialog()
        dlg.set_title("Selecione a data:")
        dia,mes,ano = list(map(int,self.__et.get_text().split("/")))
        mes-=1       
        dlg.add_button("gtk-ok",1)
        dlg.add_button("gtk-cancel",0)
        cld=Gtk.Calendar()
        cld.select_day(dia)
        cld.select_month(mes,ano)
        dlg.vbox.add(cld);dlg.show_all();
        if dlg.run():
            ano,mes,dia =cld.get_date()
            mes+=1
            self.__et.set_text("%.2i/%.2i/%s"%(dia,mes,ano))
        dlg.destroy()
#-------------------------------------------------------------------------------------------------------------------------------------------------------        
class Sumario(Frame):
    def __init__(self,parent):
        Frame.__init__(self)
        bx = Box()
        bx.set_orientation(Gtk.Orientation.VERTICAL)
        self.top=parent
        self.db=parent.db
        self.lista = TreeView()
        scw = ScrolledWindow()
        #scw.set_viewport(self.lista)
        scw.add(self.lista)
        bx.pack_start(scw,True,True,1)
        self.lista.append_column(TreeViewColumn("Descrição",CellRendererText(),text=0))
        self.lista.append_column(TreeViewColumn("Valores",CellRendererText(),text=1))
        self.lista.append_column(TreeViewColumn("Total",CellRendererText(),text=2))
        bt = ToolButton()
        bt.set_icon_name('gtk-refresh')
        bt.connect("clicked",lambda evt:self.atualizar())
        bx.pack_end(bt,False,False,1)
        self.add(bx)
        self.atualizar()
        
    def atualizar(self):
        md = ListStore(str,str,str)
        comprasparceladas=self.testa_zero(self.db.execute("SELECT total FROM somaparcelados").fetchone()[0])
        gastosfixos=self.testa_zero(self.db.execute("SELECT SUM(valor) FROM gastos_fixos ").fetchone()[0])
        gastosrecorrentes=self.testa_zero(self.db.execute("SELECT * FROM totalrecorrentes").fetchone()[0])#soma das medias dos gastos recorrentes no ano corrente
        md.append(["Compras parceladas:",formata_moeda('%.2f'%comprasparceladas),""])
        md.append(["Gastos fixos:",formata_moeda('%.2f'%gastosfixos),""])
        md.append(["Gastos recorrentes:",formata_moeda('%.2f'%gastosrecorrentes),""])
        md.append(['Total:',"",formata_moeda('%.2f'%(comprasparceladas+gastosfixos+gastosrecorrentes))])
        self.lista.set_model(md)

    def testa_zero(self,valor):#Teste se o retorno do banco de dados for none e retorna 0
        if not(valor):
            return(0)
        else:
            try:
                ret=float(valor)
            except:ret=0
            return(ret)       
#-------------------------------------------------------------------------------------------------------------------------------------------------------        
#Janela principal
class Orcamento(Window):
    def __init__(self):
        self.start_db()
        Window.__init__(self)
        self.set_title("Controle orcamentario")
        self.menu=MenuBar()
        self.make_menu()
        self.desktop=Notebook()
        self.stat=Statusbar()
        bx = Box()
        bx.set_orientation(Gtk.Orientation.VERTICAL)
        bx.pack_start(self.menu,False,True,1)
        bx.pack_start(self.desktop,True,True,1)
        bx.pack_end(self.stat,False,True,1)
        self.add(bx)
        im=Pixbuf.new_from_xpm_data(["7 9 2 1",
                " 	c None",
                ".	c #000000",
                "   .       ",
                " .....     ",
                " . . .     ",
                " ...       ",
                "  ....     ",
                "   . .     ",    
                " . . .     ",
                " .....     ",
                "   .       "])
        img=Image.new_from_pixbuf(im)
        self.desktop.append_page(Sumario(self),img)
        self.show_all()
        self.set_size_request(800,500)


    def show_parcelados(self,evt):
        fr = Parcelamentos(self)
        self.desktop.append_page(fr,Label(label="Parcelamentos"))        
        fr.show_all()
        self.desktop.set_current_page(-1)

    def show_fixos(self,evt):
        fr = Fixos(self)
        self.desktop.append_page(fr,Label(label="Gastos fixos"))        
        fr.show_all()
        self.desktop.set_current_page(-1)

    def show_recorrentes(self,evt):
        fr=GastosRecorrentes(self)
        self.desktop.append_page(fr,Label(label="Gastos recorrentes"))
        fr.show_all()
        self.desktop.set_current_page(-1)
    
    def show_movimento(self,evt):
        fr=Gastos(self)
        self.desktop.append_page(fr,Label(label="Gastos"))
        self.desktop.set_current_page(-1)

    def faturas(self,evt):
        self.desktop.append_page(Fatura(self),Label(label="Faturas de cartão de crédito"));self.desktop.set_current_page(-1)

    def eventos(self,evt):
        self.desktop.append_page(ListaEventos(self),Label(label="Lista de eventos"));self.desktop.set_current_page(-1)

    def make_menu(self):
        m = Menu()
        mit = [MenuItem(label="Tipos de lancamentos"),MenuItem(label="Gastos fixos"),MenuItem(label="Faturas Cartão Crédito"),
               MenuItem(label="Movimento"),MenuItem(label="Parcelados"),MenuItem(label="Evento"),MenuItem(label="Gastos Recorrentes"),
                SeparatorMenuItem(),ImageMenuItem(use_stock="gtk-quit")]
        mit[0].connect("activate",lambda evt:TiposLancamentos(self))
        mit[1].connect("activate",self.show_fixos)
        mit[2].connect("activate",self.faturas)
        mit[3].connect("activate",self.show_movimento)
        mit[4].connect("activate",self.show_parcelados)
        mit[5].connect("activate",self.eventos)
        mit[6].connect("activate",self.show_recorrentes)
        mit[8].connect("activate",main_quit)        
        for item in mit:m.append(item)
        M = MenuItem(label="Orcamento")
        M.set_submenu(m)
        M1=MenuItem(label="Dados")
        m1=Menu()
        mm1=MenuItem(label="Exportar dados");mm1.connect("activate",lambda evt:ExportarLancamentos(self).show_all())
        mm2=MenuItem(label="Importar dados");mm2.connect("activate",lambda evt:ImportarLancamentos(self))
        m1.append(mm1)
        m1.append(mm2)
        M1.set_submenu(m1)
        self.menu.append(M)
        self.menu.append(M1)

    def start_db(self):
        if os.name=='nt':
            pt=os.path.join(os.environ.get("HOMEDRIVE"),os.environ.get("HOMEPATH"),".db","orcamento.db")
        else:
            pt = os.path.join(os.path.abspath(os.environ.get("HOME")),".db","orcamento.db")
        self.db = sqlite.connect(pt)
        self.db.text_factory=str
        tabelas = { "lancamento":"CREATE TABLE lancamento(data DATE,descricao CHAR(100),valor FLOAT(10.2),tipo CHAR(30))",
                    "tiposlancamento":"CREATE TABLE tiposlancamento(descricao CHAR(30))",
                    "recorrente":"create table recorrente(descricao char(50),valor float(10.2), mes int(2), ano int(4))",
                    "parcelamentos":"create table parcelamentos(data date,descricao char(50),valor float(10.2),numparcelas int(3),datainicio date,datafinal date)",
                    "eventos":"create table eventos(descricao char(100),data date,obs text)",
                    "lancamento_eventos":"CREATE TABLE lancamento_eventos(evento char(100),data DATE,descricao CHAR(100),valor FLOAT(10.2))",
                    "gastos_fixos":"CREATE TABLE gastos_fixos(dia INT(2),descricao CHAR(50),valor FLOAT(10.2))",
                    "fatura":"CREATE TABLE fatura(data date,descricao char(100),valor FLOAT(10.2), aberta INT(1))"}
                
        views={"somaparcelados":"CREATE VIEW somaparcelados AS select sum(valor/numparcelas) as total from parcelamentos where datafinal>=Date()",
               "totalrecorrentes":"CREATE VIEW totalrecorrentes AS SELECT SUM(media) FROM mediarecorrentes",
               "mediarecorrentes":"CREATE VIEW mediarecorrentes AS SELECT AVG(valor) AS media FROM recorrente WHERE ano=strftime('%Y') GROUP BY descricao" }
        for nome  in self.db.execute("SELECT name FROM SQLITE_MASTER WHERE type='table' or type='view'").fetchall():
            if (not(nome[0] in tabelas) and not(nome[0] in views)):
                print(("apagando tabela %s"%nome))
                self.db.execute("DROP TABLE %s"%nome[0])
        for tabela in list(tabelas.keys()):
           if  self.db.execute("SELECT count(name) FROM SQLITE_MASTER WHERE name=?",(tabela,)).fetchone()[0]==0:
                print(("Criando %s"%tabela))
                try:
                    self.db.execute(tabelas[tabela])                    
                except Exception as e:
                    if str(e)=="no such table: main.mediarecorrentes":
                        self.db.execute(tabelas['mediarecorrentes'])
                        self.db.execute(tabelas['totalrecorrentes'])#Uma view depende da outra 
                    print(e)
        for tabela in list(views.keys()):
           if  self.db.execute("SELECT count(name) FROM SQLITE_MASTER WHERE name=?",(tabela,)).fetchone()[0]==0:
                print(("Criando %s"%tabela))
                try:
                    self.db.execute(views[tabela])                    
                except Exception as e:
                    if str(e)=="no such table: main.mediarecorrentes":
                        self.db.execute(tabelas['mediarecorrentes'])
                        self.db.execute(tabelas['totalrecorrentes'])#Uma view depende da outra 
                    print(e)

#-------------------------------------------------------------------------------------------------------------------------
#Funcoes auxiliares
def formata_moeda(valor):
    inteiro,decimal = valor.split(".")
    inteiro = split1000(inteiro)
    return('%s,%s'%(inteiro,decimal))

def split1000(s):
   return s if len(s) <= 3 else split1000(s[:-3]) + '.' + s[-3:] 

def soma_mes(data,m,formato='y-d-m'):
    if formato=='y-d-m':
        ano,mes,dia = list(map(int,data.split("-")))
    else:
        dia,mes,ano = list(map(int,data.split("/")))
    for smes in range(1,m):
        if mes==12:
            mes=1
            ano+=1
        else:mes+=1
    y = ano
    if y%100==0:y=ano/100
    if y%4==0:
        diasfev=29
    else:
        diasfev=28
    if mes==2 and dia>diasfev:
        dia=dia-diasfev
        mes+=1
    if dia==31 and mes in [4,6,9,11]:
        mes+=1
        dia=1
    return("%i-%.2i-%.2i"%(ano,mes,dia))

def conta_meses(datai,dataf):
    dias = (str2date(dataf) - str2date(datai)).days
    contador=0
    datai =str2date(datai)
    while dias>0:       
        dcorrente= dias_mes(date2str(datai))
        if (dias>dcorrente):
            dias-=dcorrente
            datai+=datetime.timedelta(days=dcorrente)
            contador+=1
        else:dias=0                 
    return(contador)

def date2str(data):
    return("%i-%.2i-%.2i"%(data.year,data.month,data.day))

def str2date(data):
    ano,mes,dia = list(map(int,data.split("-")))
    return(datetime.date(ano,mes,dia))

def dias_mes(data):
    ano,mes,dia = list(map(int,data.split("-")))
    if mes==2:
        y = ano
        if y%100==0:y=ano/100
        if y%4==0:
            return(29)
        else:
            return(28)
    if mes in [4,6,9,11]:return(30)
    return(31)
#-------------------------------------------------------------------------------------------------------------------------------------------------------        
# Leitor simplificado de OFX, capaz de ler parcialmente arquivos OFX de cartão de crédito
# ele lê apenas os lancamentos
# recebe como entrada uma string com o conteudo completo do arquivo ofx
#
class ofxParser:
    def __init__(self,dados):
        self.parse_header(dados)
        if "ENCODING" in self.header and self.header['ENCODING']!='USASCII':
            dados=dados[dados.find("<"):].encode(self.header['ENCODING'])
        self.dados=self.parse(dados)        

    def parse_header(self,dados):
        self.header={}
        for l in dados[0:dados.find("<")].split("\r\n"):
            if l.strip()!='':self.header[l.split(":")[0]]=l.split(":")[1]

    def parse(self,dados):
        indices_abertura=[]
        for it in re.finditer("<STMTTRN>",dados,re.IGNORECASE):indices_abertura.append(it.start())
        indices_fechamento=[]
        for it in re.finditer("</STMTTRN>",dados,re.IGNORECASE):indices_fechamento.append(it.start())
        indices=[]
        elementos=[]
        for pos in range(0,len(indices_abertura)):
            campo=dados[indices_abertura[pos]+9:indices_fechamento[pos]]
            data=(re.findall("<DTPOSTED>*.*</DTPOSTED>",campo)[0].replace("<DTPOSTED>","").replace("</DTPOSTED>",""))
            data=data[6:]+"/"+data[4:6]+"/"+data[0:4]
            valor=self.limpa((re.findall("<TRNAMT>*.*</TRNAMT>",campo)[0].replace("<TRNAMT>","").replace("</TRNAMT>","")))
            descricao=(re.findall("<MEMO>*.*</MEMO>",campo)[0].replace("<MEMO>","").replace("</MEMO>",""))            
            elementos.append({"data":data,"valor":valor,"descricao":descricao})
        return(elementos)

    def limpa(self,valor):
        return(float(valor.replace('.',''))/100)
#-------------------------------------------------------------------------------------------------------------------------------------------------------        
logo_xpm=["32 32 3 1",
" 	c None",
".	c #F8FF84",
"+	c #121212",
"                                ",
"                                ",
"             ..+                ",
"             ..+                ",
"             ..+                ",
"          ++....++              ",
"       ++..........++++         ",
"     +.....++  ++++....++       ",
"     +....+       ++....++      ",
"    +.....+        ++....+      ",
"   +......+         +...++      ",
"   +......++         ++++       ",
"   +......++++                  ",
"   +.........++++++             ",
"    ++...............++         ",
"     ++++............++++       ",
"       ++................++     ",
"         +  ++++++++.......+    ",
"                +++++......+    ",
"    +++++           +......+    ",
"   +......+          ......+    ",
"   +......+          .....++    ",
"   +......+         +....++     ",
"    ++....++      ++...+++      ",
"     ++..............+++        ",
"       +++++.......+++          ",
"            +..++               ",
"            +..                 ",
"            +..                 ",
"            +..                 ",
"                                ",
"                                "]
importar_xpm=["24 24 3 1",
" 	c None",
".	c #000000",
"+	c #FFFFFF",
"             ......     ",
"            ...         ",
"           ...          ",
"          ...           ",
"          ..            ",
"         ...            ",
"         ...            ",
"         ...            ",
"         ...            ",
"      .........         ",
"       .......          ",
"        .....           ",
"         ...            ",
"          .             ",
"     .......            ",
"     .+++++.            ",
"     .++++++.           ",
"     .+...+++.          ",
"     .+++++++.          ",
"     .+.....+.          ",
"     .+++++++.          ",
"     .+.....+.          ",
"     .+++++++.          ",
"     .........          "]

if __name__=="__main__":
    jn = Orcamento()
    img =Pixbuf.new_from_xpm_data(logo_xpm)
    jn.set_icon(img)
    jn.connect("destroy",main_quit)
    main()


#!-*-coding:utf-8-*-
##################################
#
import gi
gi.require_version("Gtk","3.0")
gi.require_version("Gdk","3.0")
from gi.repository import Gtk
from gi.repository.Gtk import Window,Entry,Button,Frame,MenuBar,Menu,HBox,VBox,MenuItem,SeparatorMenuItem,main,main_quit,ToolButton
from gi.repository.Gtk import ListStore,TreeView,TreeViewColumn,CellRendererText,ScrolledWindow
from gi.repository import Gdk as gdk
from gi.repository.GdkPixbuf import Pixbuf
import time,datetime
delta = datetime.timedelta


#----------------
# Objeto para lidar com as operações de boleto
#       
data_base=datetime.date(1997,0o7,10)

class Boleto:    
    def __init__(self,valor):
        try:
            linha=''
            for v in valor:
                if v in '0123456789':linha+=v #Elimina tudo que não for numero
            campo1 = linha[4:9] # 20 a 24 no codigo de barras
            campo2 = linha[10:20] # 25 a 34 no codigo de barras
            campo3 = linha[21:31] # 35 a 44 no codigo de barras
            campo4 = linha[32]
            campo5 = linha[33:]
            banco  = linha[0:3] 
            moeda = linha[3]
            self.codigo_barras=banco+moeda+campo5+campo1+campo2+campo3
            self.vencimento= self.get_vencimento(campo5[:4])
            self.valor=float(campo5[4:])/100
            self.linha_digitavel=[linha[0:10],linha[10:21],linha[21:32],campo5]
            dv = self.dv_geral(self.codigo_barras)
            self.campos = {'campo1': campo1,
                           'campo2':campo2,
                           'campo3':campo3,
                           'campo5':campo5,
                           'dv':dv,
                           'banco':banco,
                           'moeda':moeda}
            setattr(self,'keys',getattr(self.campos,'keys'))
            setattr(self,'__getitem__',getattr(self.campos,'__getitem__'))
        except Exception as e: 
            raise Exception("Erro na inicialização do objeto \n %s"%e)

    def get_vencimento(self,dias):
        d=datetime.timedelta(int(dias))
        data = data_base+d
        data = '%.2i/%.2i/%i'%(data.day,data.month,data.year)
        return(data)
            
    def dv_geral(self,ldigt): #difere do mod 11 normal porque o fator multiplicador vai até 2 e não 1 e 10,11 e 0 == 1(nao existe x)
        soma=0
        controle=9
        for i in range(len(ldigt)-1,-1,-1):
            soma=soma+(controle*int(ldigt[i]))
            if controle>2:
                controle-=1
            else:controle=9
        dv=str(soma%11)
        if dv=='10':dv='1'
        if dv=='11':dv='1'
        if dv=='0':dv='1'
        return(dv)

    def dv_mod11(self,valor):
        soma=0
        controle=9
        for i in range(len(valor)-1,-1,-1):
            soma=soma+(controle*int(valor[i]))
            if controle>1:
                controle-=1
            else:controle=9
        dv=str(soma%11)
        if dv=='10':dv='X'
        return(dv)

    def dv_mod10(self,field):       
        mult = 2
        sum = 0 
        for i in range(len(field)-1, -1, -1):
            x = (int(field[i]) * mult)	 
            if(x > 10):
               x = (x % 10) + 1	   
               sum += x	 
            if(mult == 2):
                mult = 1	   
            else:
                mult = 2	 
        if(not (sum % 10)):
            sum = 0	 
        else:
            sum = 10 - (sum % 10)	 
        return str(sum)
#----------------------------------------------------------------
#Inicio da interface grafica
class Cboleto(Window):
    def __init__(self):
        Window.__init__(self)
        self.set_title("Boletos")
        bx = VBox(False,False)
        bx.pack_start(self.make_menu(),False,True,0)
        fr = Frame(label="Linha digitavel:")
        b = HBox(False,False)
        self.ldigitavel=Entry()
        b.pack_start(self.ldigitavel,True,True,0)
        bt = ToolButton("gtk-refresh")
        bt.connect("clicked",lambda evt:self.processar())
        b.pack_end(bt,False,False,0)
        fr.add(b)
        bx.pack_start(fr,False,True,0)
        #
        bx2=HBox(False,False)
        self.dados =  TreeView()
        self.dados.append_column(TreeViewColumn("Campo",CellRendererText(),text=0))
        self.dados.append_column(TreeViewColumn("Valor",CellRendererText(),text=1))
        self.dados.set_model(ListStore(str,str))
        scw = ScrolledWindow();scw.add(self.dados)
        bx.pack_start(scw,True,True,0)
        self.add(bx)
        self.set_size_request(600,600)
        self.boleto=None

    def make_menu(self):
        mb=MenuBar()
        m = Menu()
        mi = [MenuItem("Mudar a data vencimento"),SeparatorMenuItem(),MenuItem("Sair")]
        mi[-1].connect("activate",main_quit)
        mi[0].connect("activate",lambda evt=None:self.mudar_data())
        for i in mi:m.add(i)
        base = MenuItem("Boletos")
        base.set_submenu(m)
        mb.add(base)
        return(mb)
        
    def processar(self):
        try:
            boleto = Boleto(self.ldigitavel.get_text())
            md = ListStore(str,str)
            md.append(['Campo1',boleto.linha_digitavel[0]])
            md.append(['Campo2',boleto.linha_digitavel[1]])
            md.append(['Campo3',boleto.linha_digitavel[2]])
            md.append(['Digito verificador geral',boleto['dv']])        
            md.append(['Campo5',boleto.linha_digitavel[3]])
            md.append(["Valor",'%.2f'%boleto.valor])
            md.append(["Data vencimento",boleto.vencimento])
            self.dados.set_model(md)
            self.boleto=boleto
        except Exception as e:
            dlg=MessageDialog(parent=self,buttons=BUTTONS_OK,type=MESSAGE_ERROR)
            dlg.set_markup(str(e))
            dlg.run();dlg.destroy()

    def mudar_data(self):
        if self.boleto==None:return
        dlg=Dialog()
        fr = Frame("Data:")
        data= DatePicker()
        fr.add(data)
        dlg.vbox.pack_start(fr,False,True,0)
        dlg.add_buttons('gtk-ok',1,'gtk-cancel',0)
        dlg.show_all();
        if dlg.run():
            pass
        dlg.destroy()

class DateEntry(Entry):
    def __init__(self):
        Entry.__init__(self)
        self.connect("insert_text",self.validar)
        self.connect("key_press_event",self.testa_comandos)

    def testa_comandos(self,entry,evento):#Teclas de atalho
        keyname = gdk.keyval_name(evento.keyval)
        if keyname.lower()=='delete':self.set_text('')
        
    def validar(self,entry,text,*args):      
        entry.stop_emission("insert_text")#evita o processamento normal do sinal
        result = ''.join([c for c in text if c in '0123456789'])
        texto=self.formata((entry.get_text()+result))
        entry.handler_block_by_func(self.validar)#Bloqueia o sinal para evitar recursividade
        entry.set_text(texto)
        entry.handler_unblock_by_func(self.validar)

    def formata(self,texto):
        texto=''.join([c for c in texto if c in '0123456789'])
        if len(texto)>=2:
            texto=texto[0:2]+'/'+texto[2:]
        if len(texto)>=5:
            texto=texto[0:5]+'/'+texto[5:]
        if len(texto)>10:texto=texto[0:10]
        return(texto)


class DatePicker(Frame):
    def __init__(self,text=''):
        if text=='':
            Frame.__init__(self)
        else:Frame.__init__(self,text)
        bx = HBox(False,0)
        self.__et =DateEntry()
        #self.__et.set_editable(False)
        image = Image()
        image.set_from_pixbuf(gdk.pixbuf_new_from_xpm_data(["16 16 9 1",
                    " 	c None",
                    ".	c #000000",
                    "+	c #E012EA",
                    "@	c #8F599F",
                    "#	c #FFFFFF",
                    "$	c #9A2424",
                    "%	c #45C174",
                    "&	c #F9F609",
                    "*	c #48CFDB",
                    "                ",
                    " .............. ",
                    " .+++++++++++@. ",
                    " .@@@@@@@@@@@@. ",
                    " .............. ",
                    " .############. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .##*###*##*##. ",
                    " .#$#%$&#$%#$#. ",
                    " .############. ",
                    " .............. ",
                    "                "]))
        bt =ToolButton(image)
        bt.connect("clicked",self.__set_date)
        bx.pack_start(self.__et,True,True)
        bx.pack_end(bt,False,False)
        self.add(bx)
        self.__et.set_text(time.strftime("%d/%m/%Y"))
        self.set_shadow_type(SHADOW_NONE)

    def set(self,data):
        ano,mes,dia = data.split('-')
        data = '%s/%s/%s'%(dia,mes,ano)
        self.__et.set_text(data)

    def get(self):
        return(self.get_date('y-m-d'))

    def get_date(self,format="d/m/y"):
        '''possible date formats d/m/y y-m-d '''
        dia,mes,ano=self.__et.get_text().split("/")
        return(format.replace("d",dia).replace("m",mes).replace("y",ano))
                
    def __set_date(self,evt):
        dlg=Dialog()
        dlg.set_title("Selecione a data:")
        dia,mes,ano = list(map(int,self.__et.get_text().split("/")))
        mes-=1       
        dlg.add_button("gtk-ok",1)
        dlg.add_button("gtk-cancel",0)
        cld=Calendar()
        cld.select_day(dia)
        cld.select_month(mes,ano)
        dlg.vbox.add(cld);dlg.show_all();
        if dlg.run():
            ano,mes,dia =cld.get_date()
            mes+=1
            self.__et.set_text("%.2i/%.2i/%s"%(dia,mes,ano))
        dlg.destroy()

icone=["24 24 3 1",
" 	c None",
".	c #FFFFFF",
"+	c #000000",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"  ..................... ",
"  ...              + +. ",
"  ..++++++++++++++ + +. ",
"  .................+.+. ",
"  ..+++++++++++...++.+. ",
"  ..................++. ",
"  ..+++................ ",
"  ......++.++.+.+.+.+.+ ",
"  ......+...+.+.+.+.+.+ ",
"  ......+.+.++..+.+.+.+ ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        ",
"                        "]
        
if __name__=="__main__":
    jn=Cboleto()
    img =Pixbuf.new_from_xpm_data(icone)    
    jn.set_icon(img)
    jn.connect("destroy",main_quit)
    jn.show_all()
    main()
